%In both steady-state models included in our analysis, K+09 and S+14 models, the
%predicted hydrogen volume density depends on the cloud-incident free space
%far-ultraviolet (FUV) radiation field (measured by $I_{UV}$ and $G_0^\prime$ in
%the S+14 and K+09 models respectively). It is thus in our interest to estimate
%the inter-stellar radiation field (ISRF) strength incident upon each cloud in
%order to accurately predict volume densities from the steady-state models. We
%use global cloud dust temperatures to estimate the incident ISRF strength on
%each cloud.


One important parameter for interpretation of our results, especially when
trying to test different model predictions, is the strength of the external
inter-stellar radiation field (ISRF) incident upon our core regions.  We derive
this external ISRF from the Planck dust temperature and the emissivity index
images \citep{planck13_11}.  Both the dust temperature and the emissivity index
were estimated via SED fitting and by assuming a single dust population along
the line-of-sight. The authors estimated the uncertainties of the temperatures
by running monte carlo simulations with varying contributions of the cosmic
infrared background and random noise at varying resolution.

% WE SHOULD TALK HERE ABOUT DUST TEMP ACROSS GMCS.
%Figure \ref{fig:rad_field_map} shows the radiation field map with our
%large-area GMC boundaries shown. 

%It is striking how Taurus has a higer abundance
%of colder dust relative to Perseus, and even California. In addition, while
%Perseus appears embedded in a relatively uniform ISRF, there are larger spatial
%variations in the case of Taurus and California, and some evidence for diffuse,
%possibly foreground, material at $\sim16$ K.  HOW DOES THIS COMPARE WITH
%PREVIOUS STUDIES??

To estimate the strength of the ISRF we use the following equation assuming
large interstellar silicates \citep{galliano11}:

\begin{equation}
  U_{\rm M83}= \left ( \frac{T_{\rm dust}}{17.5\,{\rm K}} \right )^{(\beta +4)}
\end{equation}

\noindent where $U_{\rm M83}$ is the \citet{mathis83} ISRF (integrated from
0.09\,\micron\ to 8\,\micron).  By using $T_{\rm dust}$ and $\beta$ images, we
derive $U_{\rm M83}$ on the pixel-by-pixel basis, and then estimate median value
of $U_{\rm M83}$ for each one of the GMCs as a measure of the  external ISRF
incident upon the GMCs which heats up dust grains. This analytic relation gives
a comparable measure of $U_{\rm M83}$ to numerical experiments for $U_{\rm M83}
< 10^4$ at which point the dust absorption efficiency changes and the dust
temperature rises faster with increasing radiation strength \citep{draine11}.

We measure median ISRFs within each region shown in
\autoref{fig:multicloud_map}.  We then run 1,000 Monte Carlo simulations to
assess uncertainty introduced by our region selection and in the dust
temperature and emissivity index. In each simulation, we add a
normally-distributed random offset of scale 1\degree\ to each vertice in both
right ascension and declination. We also add a random-normal offset to $T_{\rm
dust}$ and $\beta$ for each sightline with scale equal to the uncertainty in
$T_{\rm dust}$ and $\beta$.

We report the ISRF, as well as the dust temperature, and the associated
uncertainties in \autoref{table:cloud_properties}.  We find the median dust
temperatures to be 17.16, 18.06 and 17.31\,K, respectively for California,
Perseus and Taurus. These are essentially estimates of dust temperatures in the
far outskirts of the three GMCs. \autoref{fig:rad_field_map} shows the radiation
field map with our large-area GMC boundaries shown. We find small variation in
the radiation field, from $\sim$\,0.3\,Mathis fields in the densest cold cores
to $\sim$\,10\,Mathis fields in regions with internal star formation such as the
cluster in California, L1482. Our measure of the ambient ISRF, the median value
of ISRF sightlines in each region, is insensitive to these large variations.

Finally, to relate $U_{\rm M83}$ with other measures of the ISRF we adopt the
following conversions.  $U_{\rm M83}$ is 1.14 times stronger than the Habing
field $G_0$, which is measured in the range of 6-13.6 eV and is normalized to
$5.29\times 10^{-14}$ erg cm$^{-3}$ \citep{draine11}.  Therefore, to express the
strength of the ISRF in Habing units we need to multiply $U_{\rm M83}$ by 1.14.
Similarly, far ultra-violet (FUV) background field estimated by \citet{draine78}
is roughly 1.69 times stronger than the Habing field. Therefore, to express the
strength of the ISRF in the units of Draine (1978) field, we need to multiply
$U_{\rm M83}$ by 1.48. The median radiation fields and the 68\% confidence
uncertainty on the median in the three clouds are $0.60\,\pm\,0.01$,
$0.81\,\pm\,0.01$, and $0.64\,\pm\,0.01$\,aDraine fields for California,
Perseus, and Taurus. These radiation field strengths reflect the external ISRF
incident upon the GMCs which heat up dust grains, or rather the ambient ISRF
assumed in the S+14 and K+09 models. The uncertainties on the radiation fields
assume the median of each LOS radiation measure in a region accurately
represents the radiation field, e.g., the uncertainties do not account for
systematic inaccuracies.

See \autoref{table:cloud_properties} for the derived free-space FUV radiation
fields for each cloud relative to both the Draine and the Mathis fields.
\citet{flagey09} found a Draine field between 0.3 and 3.2 in 6 fields in Taurus
using Spitzer observations to estimate grain heating from incident radiation
field. Our estimated ISRF of 0.81 Draine fields is in agreement with these
observations. \citet{lee12} derived a Habing field of  0.8 in Perseus, however
our estimated ISRF is 1.20 Habing fields. These differences could be due to mean
vs. median calculations, or region choices, or from differences in IRAS vs.
Planck. The latter is unlikely because the Planck map was calibrated with the
IRAS observations \citep{planck13_11}. 

%WILL ASK MIN ABOUT THESE DIFFERENCES.  WE
%SHOULD DISCUSS THESE VALUES.

  \begin{figure*}[!ht]
    \begin{center}
    \includegraphics[width=\linewidth]{figures/multicloud_rad_field_map.pdf}

    \caption{\label{fig:rad_field_map} The radiation field in units of Mathis
      field of California, Perseus and Taurus. We derived the radiation field by
      assuming the large dust grains are in thermal equilibrium with the
      interstellar radiation field, then used the dust temperature and
      emissivity index from Planck to calculate the strength of the field
      \citep{planck13_11}. The white regions are the same regions bounding each
      cloud shown in \autoref{fig:multicloud_map}. We adopt the median radiation
      field strength within each cloud region as the ambient radiation field
      strength used in both the S+14 and K+09 models.  
    }

    \end{center}
  \end{figure*}


%$U_{\rm M83}$ is the \citet{mathis83} ISRF (integrated from
%0.09\,\micron\ to 8\,\micron). The Mathis et al. ISRF is related to
%the Habing's UV ISRF (6-13.6 eV) with $G_0=1.14$.


%TO BE REMOVED/MODIFIED: \noindent where $U_{\rm M83}$ is the ISRF by \citet{mathis83} integrated from
%0.09\,\micron\ to 8\,\micron. Specifically, $U_{\rm M83}$ = 1 =
%0.022\,\fluxunit. This relation is based on many observations showing that large
%interstellar silicates have an equilibrium temperature of 17.5\,K in the solar
%neighborhood (e.g.  \citealt[][]{boulanger96}). 
%Finally, the free-space far ultra-violet (FUV) intensity relative to the Draine
%field, $I_{UV}$ is 1.5 times stronger in the FUV range of 6\,-\,13.6\,eV range
%than $U_{\rm M83}$. The K+09 model adopts a measure of the free-space FUV
%intensity, $G_0$, relative to the Habing field \citep{habing68}. The Draine
%field is roughly 1.69 times stronger than the Habing field at 1000\,\angstrom\
%(the center of the relevant FUV range).


\begin{comment}

--- 

Here U(M83) is the ISRF by Mathis+83
integrated from 0.09 micron to 8 micron.  To be specific, U(M83) = 4pi * \int
J(nu) d(nu) (0.09 micron ~ 8 micron) and U(M83) = 1 corresponds to 2.2e-5 W m-2.
--- For Perseus with T_dust ~ 17 K, U(M83) ~ 0.8.  --- But what we need to know
is I_UV, the scaling factor for the Draine field.  For the FUV range of 6 ~ 13.6
eV, the Draine field is a factor of ~1.5 stronger than the Mathis field.  Based
on all these estimates, I would expect I_UV ~ (1/1.5) * 0.8 ~ 0.5 for Perseus.
--- Another thing I want to mention is that the "empirical" relation I used
above is independent from the dust (LW photon) absorption cross section.  Please
let me know how you think of my estimate of I_UV. 


\end{comment}

\begin{comment}

The radiation field measured based on dust SED fitting actually refers
to the radiation incident upon the surface of Taurus.  This is
different from the free-space radiation field. For an optically thick
object (like Taurus), the free-space radiation field would be twice the
radiation field on the surface of the object.  For example, the
radiation field inferred from the dust temperature distribution across
Perseus is 0.4 Draine field.  So the free-space radiation field, which
is what KMT requires as an input for n\,(CNM), would be 2 * 0.4 Draine
field $\sim$1 Draine field. 

\end{comment}





