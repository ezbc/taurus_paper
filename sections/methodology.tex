
\subsection{Cloud Boundaries}
\label{sec:cloud_boundaries}

  \begin{figure*}[!th]
    \begin{center}
%    \includegraphics[width=\linewidth]{figures/multicloud_av_nhi_map.pdf}
    \includegraphics[width=\linewidth]{figures/av_map.pdf}

    \caption{\label{fig:multicloud_map} Original Planck $A_V$ map of the field
      containing California, Perseus and Taurus.  The white outlines represent
      the boundaries of each region we chose. Our criteria for choosing cloud
      regions was to confine the clouds to regions where the background $A_V$
      gradient did not vary greatly, and which included diffuse $A_V$ regions.  
      }

    \end{center}
  \end{figure*}

  While Perseus, California and Taurus belong to the Taurus-Auriga-Perseus
  complex of dark and star-forming clouds, they are located at well-separated
  distances.  We therefore start by introducing rough GMC boundaries and in
  further analysis treat each GMC as a separate entity.  In
  \autoref{fig:multicloud_map} we show the original Planck-$A_V$ map of the
  whole complex based on the coverage of the GALFA-\hi\ survey. We overlay cloud
  boundaries used in our study, which roughly follow the  regions identified in
  \citep{lombardi10}.  Our main criteria are 1) confine the GMCs to regions
  where the background $A_V$ does not vary significantly, 2) include as much of
  the diffuse $A_V$ as possible to probe GMC outskirts.  
  %In the case of Taurus, we include the diffuse filamentary structures which
  %appear to be associated with the cloud e.g., Taurus' North-Western arm.

\subsection{Determining \nhi}
\label{sec:nhi_coldens}

  We determine \nhi\ for each GMC by integrating \hi\ under the optically-thin
  assumption over a velocity range associated with the GMC.  We inspect the bulk
  \hi\ properties to determine associated \hi\ of each cloud using median \hi\
  spectra, see \autoref{fig:spectra}. Similarly to \citet{imara11}, we fit
  multiple Gaussian functions to the median \hi\ spectrum of each GMC to
  quantify associated \hi. In the case that thermal and turbulent broadening
  define \hi\ line profiles, Gaussian functions should fit \hi\ spectra
  reasonably well. The whole Taurus-Auriga-Perseus molecular cloud complex is
  located below the Galactic plane, resulting in less complex and confused \hi\
  spectra relative to many other GMCs \citep{imara11,lee12}.  
  
  We identify the associated \hi\ velocity range using \hi\ Gaussian components
  near in velocity to the CO emission of the GMC. We use the smoothed GALFA-\hi\
  data with 8\farcm4 resolution in order to compare to the CfA CO data. We fit
  as few Gaussian functions as possible to accurately reproduce the spectra
  (shown as dashed lines). In the case of Perseus and Taurus we use two Gaussian
  functions, California requires four functions. See \autoref{fig:spectra} for
  the fitted Gaussian functions to the median \hi\ spectra. The median
  $^{12}$CO(1-0) spectrum is shown in orange.  We assume that the Gaussian
  function that is nearest in velocity to the CO peak is associated with the
  GMC, and  include all \hi\ within two standard deviations ($\pm 2\,\sigma_{\rm
  HI}$) of this Gaussian component in our \hi\ velocity range following
  \citet{imara11}.  In the case of Perseus and Taurus  these selection criteria
  include most of the velocity range occupied by the main \hi\ component (shown
  as gray region), while the secondary very broad \hi\ component that extends to
  high negative velocities is excluded as it does not have any corresponding CO
  emission.  However, California is more complex, which is consistent with it
  being the closest to the Galactic plane. It has two clear CO components
  \citep{kong15}, and the \hi\ profile is even more extended.  We consider the
  two \hi\ Gaussian components that are the closest in velocity to the two CO
  components to be associated with California. By combining the $\pm
  2\,\sigma_{\rm HI}$ velocity range of those two \hi\ components we estimate
  the velocity range shown in \autoref{fig:spectra}. 

  \autoref{table:cloud_properties} lists the \hi\ velocity range for each GMC.
  In all cases, the velocity range is similar to the range found by
  \citet{lee12} and \citet{imara11} for Perseus. This is encouraging considering
  that different methods were used in these studies.  For example, \citet{lee12}
  estimated $-5$ to $+15$\,\kms, based on the spatial correlation between \nhi\
  and $A_V$, while \citet{imara11} estimated $-8$ to $+14$\,\kms\ based on the
  spatial variation of \hi\ profiles and Gaussian fitting.  The \hi\ column
  density \nhi\ was calculated by integrating over the selected velocity range
  for each GMC and under the optically-thin assumption.

  We consider two sources of uncertainty to estimate the final uncertainty
  $\delta_V$ of the selected velocity ranges. First, we selected the velocity
  range by considering peaks in \hi\ and CO emission which are usually not
  aligned in velocity. Turbulence can cause this offset between \hi\ and CO
  emission. The offset corresponds to the minimum uncertainty in the selected
  velocity range which we estimate as the absolute difference between the peak
  velocities of the corresponding \hi\ and CO Gaussian components. Second, by
  including \hi\ beyond the CO velocity extent we could be overestimating the
  neutral gas associated with GMCs, e.g., including gas within $3\,\sigma_{\rm
  HI}$ rather than $2\,\sigma_{\rm HI}$. Therefore, the maximum uncertainty in
  the selected velocity range can be obtained as a difference between our
  selected velocity range and the velocity range occupied only by CO. We
  identify the velocity range occupied by CO as velocities above $3\,\sigma$ CO
  uncertainties. [-4,\,+8], [-1,\,+10] and [+4,\,+8]\,\kms\ for California,
  Perseus and Taurus respectively. We finally estimate the uncertainty in the
  \hi\ velocity range, $\delta_V$, as the average between these minimum and
  maximum uncertainties. This uncertainty is the uncertainty on the total \hi\
  velocity range, not on each interval edge. Our derived \hi\ velocity range
  uncertainties shown in \autoref{table:cloud_properties} are moderate, on order
  of 50\% uncertainty.  Such uncertainty in the \hi\ velocity range uncertainty
  leads to a 10--20\% uncertainty in \nhi. 
  
  We estimate the total uncertainty on \nhi\ as a combination of random and
  systematic uncertainty. The uncertainty of the \hi\ velocity range leads to
  systematic uncertainty in \nhi. The uncertainties of \nhi\ for two separate
  LOS are correlated with one another. We also consider the random uncertainty
  of \nhi\ based on observational uncertainties. We estimate the
  root-mean-squared (RMS) uncertainty of each spectrum where the brightness
  temperature, $T_B$, is dominated by random fluctuations, velocities between
  90\,\kms\ and 110\,\kms. We then scale the RMS of each spectrum based on the
  increased receiver noise from higher $T_b$ by $(T_{\rm sys} + T_B) / T_{\rm
  sys}$, where $T_{\rm sys}$ is the system temperature, about 30\,K for the ALFA
  receiver. Finally, we calculate the random uncertainty of \nhi\ by summing the
  spectra uncertainties in quadrature. The median random uncertainty on \nhi\ is
  about 0.01\,\colDens\ for each cloud.
  
  \begin{figure}[!ht]
    \begin{center}
    \includegraphics[width=\linewidth]{figures/multicloud_spectra.pdf}

    \caption{\label{fig:spectra} Median \hi\ spectra of each GMC (solid black
      line) and the median CO profile (the orange solid line). The dashed black
      lines are the Gaussian components fitted to the median \hi\ spectrum. The
      thicker black dashed line represents the components used to estimate the
      final \hi\ range of each GMC. The thick reddish-orange line is the fit to
      the median \hi\ spectrum. The width of the gray regions corresponds to our
      selected \hi\ velocity range used for further analysis.
    }

    \end{center}
  \end{figure}


\subsection{DGR and Uncertainty}
\label{sec:determining_dgr}

%PLEASE CHECK FOR OBSOLETE SUB-SECTIONS.

  We derive \nhtwo\ from $A_V$ assuming a single dust-to-gas ratio (DGR) for
  each GMC with the relation
  
  \begin{equation}\label{eq:av_dgr}
    A_V = {\rm DGR}\times [2 N({\rm H}_2) + N({\rm H}\,\textsc{i})].
  \end{equation}

  Ultra-violet absorption measurements of \htwo\ support this simple assumption
  \citep{rachford09}. In the case where the line-of-sight (LOS) contains no
  molecular hydrogen, the dust column simply traces the \hi\ column scaled by
  the DGR\@. A linear fit to the relationship between the dust column density
  and \nhi\ will give the DGR\@. The leftover dust column density unassociated
  with \nhi\ scales with \nhtwo\ by the DGR. 

  We calculate the DGR by performing a fit to the relationship between
  bootstrapped Planck $A_V$ and \nhi\ shown in \autoref{fig:av_vs_nhi}.  As we
  expect that \hi\ traces the total gas column density in diffuse, primarily
  atomic regions, the fitted slope  $A_V$/\nhi\ is a good measure of DGR.

  As \autoref{fig:av_vs_nhi} shows, the vast majority of lines-of-sight appear
  to demonstrate a linear trend between $A_V$ and \nhi. The presence of \htwo\
  dominate the excursions from a linear relation at high $A_V$.  This figure
  also shows that while Perseus and California have a narrow range of \nhi\
  (variations by a factor of three at most), Taurus has a broader \nhi\
  distribution.  In addition, for Perseus and California, when \nhi\ reaches
  values of $\sim\,10\times 10^{20}$ cm$^{-2}$, the linear relation between
  $A_V$ and \nhi\ breaks due to the existence of molecular gas.  The situation
  appears more complex in the case of Taurus where high-$A_V$ excursions appear
  over a wide range of \nhi\ suggesting more significant spatial variations of
  physical properties and/or geometry/dynamics across this GMC.

  We follow a similar method to \citet{leroy09} where we perform Monte Carlo
  simulations to calculate the DGR under various realizations of the data
  assuming uncertainties in both $A_V$ and \nhi. In each simulation of the data
  we perform an orthogonal-distance-regression fit with the \texttt{scipy.odr}
  Python package to the $A_V$-\nhi\ relationship. We use the variances of the
  $A_V$ and the \nhi\ for the weights of the fit. For each simulation we
  incorporate as many uncertainties (both systematic and random) in the data as
  possible with the following steps:

  \begin{enumerate}
    
    \item Add random normal noise to each $A_V$ pixel based on random
      uncertainties of the $\tau_{353}$ image and $R_V$
      (\autoref{sec:data_planck_av}).

    \item Subtract the background from and scale $A_V$. Add a single random
      normal offset of scale 0.2\,mag to the background-subtracted $A_V$ image
      to account for the systematic uncertainty associated with the process of
      background-subtraction (\autoref{sec:dust_coldens}). Divide the Divide by
      the scalar presented in \autoref{table:planck_2mass} with a random normal
      offset on the scalar to account for the uncertainty in the scalar.   

    \item Derive \nhi\ by accounting for the uncertainty in the \hi\ velocity
      range, $\delta_V$ (\autoref{table:cloud_properties}). Add random normal
      noise to each pixel based on random uncertainties of \nhi\
      (\autoref{sec:nhi_coldens}).

  \end{enumerate}

  We run 20,000 simulations. For each data realization we perform an
  orthogonal-distance-regression without an intercept, using the random
  variances of the \nhi\ and $A_V$ data as weights. It is worth noting that the
  median error for data points above $A_V\sim5$\,mag is 1.5\,mag, while for
  $A_V<5$\,mag the median error is 0.5\,mag. Therefore, the low $A_V$ data
  points largely dominate the fit. However, to ensure that DGR calculation is
  not affected by high-$A_V$ outliers, we follow the methods of \citet{chen15}
  and \citet{sandstrom13}, we resample the data in each simulation. Given the
  empirical distribution of our observed data, we can quantify the accuracy of
  our fitted DGR to sample estimates. After simulating \nhi\ and $A_V$ in a
  simulation, we fit the DGR to resampled data. The resampled data is attained
  by randomly drawing the same number of line-of-sights (LOSs) from the original
  data, allowing the same LOS to be drawn more than once. This method, called
  bootstrapping, quantifies the effect of outliers, by simulating new samples
  from a distribution of data, potentially retaining an outlier in one
  simulation and removing the outlier the next.

  We report the median and 68\% confidence interval on the distribution of
  simulated DGRs in \autoref{table:cloud_properties}. We confirm that 20,000
  simulations accurately measures the best-estimate and uncertainty of the DGR
  by performing two more batches of 20,000 simulations. All derived
  best-estimates and uncertainties of the DGR between the three batches are
  within 1\% of each other, assuring us 20,000 simulations accurately samples
  the uncertainties of the data. \autoref{table:cloud_properties} summarizes the
  Monte Carlo simulations results. The distribution of the observed $A_V$ vs.
  \nhi\ and the fitted DGR are shown in \autoref{fig:av_vs_nhi}. 

  We find a similar DGR for Perseus, $9.4^{+2.2}_{-1.9}$\,\dgrunit, with the DGR
  found in \citet{lee12} of 11\,\dgrunit. Within our DGR uncertainty our results
  agree with \citet{lee12}. Our derived DGR for Taurus is a little less than two
  times higher than found by \citet{paradis12}, while for California we find the
  DGR to be similar to the typical Galactic value $5.3\times 10^{-22}$ mag
  cm$^2$ (e.g. \citealt{bohlin78}). Our DGR for all three GMCs is within the
  range of DGRs found within the Galaxy using UV absorption measurements
  \citep{kim96}.

  \begin{figure}[!ht]
    \begin{center}
    \includegraphics[width=\linewidth]{figures/multicloud_av_vs_nhi.pdf}

    \caption{\label{fig:av_vs_nhi} $A_V$ vs. \nhi\ for each cloud. The contours
      include 99\%, 98\%, 95\%, 86\%, and 59\% of the data. The median
      bootstrapped fit using all data points is shown as a orange-dashed line.
      The $A_V$ is derived from the Planck $A_V$ bootstrapped to the 2MASS $A_V$
      for each cloud. The median values are shown as brown squares. Our estimate
      of the DGR is insensitive to outliers, i.e. the high-$A_V$ pixels because
      of the bootstrap application. The median $A_V$ uncertainty above $A_V$ of
      5\,mag is 1.5\,mag, while below $A_V$ of 5\,mag the median error is
      0.5\,mag. The random uncertainty of \nhi\ is nearly constant for all data
      points, $\sim\,$0.02\,\colDens.}

    \end{center}
  \end{figure}
  
  \begin{deluxetable}{ l c c c }[!ht]
    \tabletypesize{\footnotesize}
    \tablecolumns{4}
    %\tablewidth{0pt}
    \tablecaption{Cloud Properties\label{table:cloud_properties}}

    \tablehead{\colhead{Property} & 
           \colhead{California} &
           \colhead{Perseus} &
           \colhead{Taurus}
         }

    \startdata{}\noindent

      \input{tables/cloud_properties.tex} 

    \enddata{}
    %\vspace{-0.8cm}
    \tablecomments{\hi\ range determined from fitting Gaussians to cloud median
      \hi\ spectrum \citep{imara11} (\autoref{sec:nhi_coldens}).  Median
      Dust-to-Gas Ratio (DGR) determined from from Monte Carlo simulations with
      68\% confidence uncertainties.  $T_{\rm dust}$ average core region dust
      temperature measured from a modified SED fit from Planck IR maps along
      each LOS.  $U_{M83}$ is the Lyman-Werner band (912\,-\,1108\,\angstrom)
      radiation field intensity relative to the Habing field, determined by
      assuming an average dust size and composition, and relating the dust
      temperature to the absorbed ambient radiation. $I_{UV}$ is the same as
      $U_{M83}$ except relative to the Draine field.  
    }

  \end{deluxetable}

\subsection{H$_2$}

  We derive \nhtwo\ using \nhi\ and the measured DGR in
  \autoref{sec:determining_dgr}. 
%the DGR in \autoref{eq:av_dgr}. 
  We then convert the \hi\ and \htwo\ column densities to surface densities by 

  \begin{equation}\label{eq:nhi_to_hisd}
    \begin{split}
      \Sigma_{\rm H\,\textsc{i}}\,[{\rm M_\odot~pc^{-2}}] & = 
    \frac{N\,{(\rm H\,\textsc{i})}}{1.25 \times 10^{20}\,[{\rm cm}^{-2}]} \\
      \Sigma_{\rm H_2}\,[{\rm M_\odot~pc^{-2}}] & = 
      \frac{N\,({\rm H_2})}{6.25 \times 10^{19}\,[{\rm cm}^{-2}]} .\\
    \end{split}
  \end{equation}
  
  \begin{figure*}[!ht]
    \begin{center}
    \includegraphics[width=\linewidth]{figures/hi_h2_maps.pdf}

    \caption{\label{fig:hi_h2_maps} \hisd\ (left) and \htwosd\ (right) for each
      cloud. The contours for \hisd\ are 5 and 10\,\msunpc, and 0, 10, and
      20\,\msunpc\ for \htwosd. We derived \hisd\ by integrating \hi\ over the
      velocity range shown in \autoref{table:cloud_properties}. We solved for
      \htwosd\ by bootstrapping Planck infrared $A_V$ to 2MASS
      stellar-extinction $A_V$, and applied the DGR presented in
      \autoref{table:cloud_properties} and \hisd\ to \autoref{eq:av_dgr}. The
      large area of negative \htwosd\ is also apparent, suggesting the DGR may
      not be constant across the entire cloud region. The minimum, median and
      maximum of \hi\ are 6.1, 10, and 14\,\msunpc\ for California, 3.8, 7.5 and
      13\,\msunpc\ for Perseus, and 2.6, 6.8, and 15\,\msunpc\ for Taurus. The
      minimum, median and maximum of \htwo\ are -7, 5, and 124\,\msunpc\ for
      California, -3, 0.2, and 294 for Perseus, and -6, 1.5, and 202\,\msunpc\
      for Taurus. The median \hisd\ systematic uncertainties for California,
      Perseus and Taurus are 1.5, 0.8 and 1.2\,\msunpc\ respectively. The median
      \hisd\ systematic uncertainties for California, Perseus and Taurus are 13,
      10, and 12\,\msunpc\ respectively.
      }

    \end{center}
  \end{figure*}

  \noindent Note that the contribution from helium is not included. 
  The derived \hisd\ and \htwosd\ maps of the three clouds are presented in  
  %We present the \hisd\ and \htwosd\ maps for each cloud in
  \autoref{fig:hi_h2_maps}. These maps demonstrate the nearly constant \hisd\
  across each cloud, which only varies within a factor of three or less. The
  \htwosd\ varies by at least a factor of 10. We derive a similar \htwosd\ map
  for Perseus to \citet{lee12}. We also find a somewhat extended \htwosd.
  5\,\msunpc\ surface densities are present out to $\sim\,0.5^\circ$, or about
  60\,pc, from the cores.

  Each cloud shows large swaths of negative \htwosd. The percent of negative
  \htwosd\ LOSs is 31\%, 45\% and 35\% for California, Perseus and Taurus
  respectively. These negative \htwosd\ fractions are somewhat larger than the
  24\% negative \htwosd\ LOSs found by \citet{lee12} for Perseus. We find that
  the distribution of \htwosd\ around 0\,\msunpc\ is symmetric for Taurus and
  Perseus, consistent with white noise without any background. However,
  California shows a bimodal distribution of \htwosd, with the peak of one
  distribution at $\sim$\,-4\,\msunpc\ and the peak of the another
  distribution at $\sim$\,6\,\msunpc. This suggests that the assumption of a
  constant background subtraction may not be ideal. However only a small
  fraction of these pixels would be considered significant given our
  uncertainties.

  % see https://ezbc.me/research/2015/08/19/bootstrapping/#background-cloud for
  % a discussion on the background cloud subtraction

  We find that a minute fraction of these negative LOSs are greater than our
  uncertainty. The median random uncertainty of \nhtwo\ is about 3.7, 0.9 and
  1.8\,\colDens\ for California, Perseus and Taurus respectively, and median
  systematic uncertainties of \nhtwo\ at 8.3, 6.1 and 7.8\,\colDens\. Likely,
  the systematic uncertainty in \nhi\ dominates the uncertainty in our derived
  \nhtwo.  $\sim$\,20\% of these negative LOSs are below 1\,$\sigma$
  uncertainties, and less than 2\% negative LOSs are below 3\,$\sigma$ in
  California. These negative LOSs are present in large regions of negative LOSs,
  meaning \nhtwo\ is subject to a systematic uncertainty. Most likely changes in
  the DGR or \nhi\ velocity range across the change are leading to negative
  \nhtwo\ LOSs. 

  A non-constant DGR could arise from dust coagulation in dense regions.
  \citet{roman-duval14} suggests coagulation of heavy elements onto dust grains
  or turbulent clustering of dust grains in the dense phase may lead to
  increased FIR emissivity. The Planck color-excess maps do account for a
  changing dust emissivity, $\beta$, however they assume a single dust
  population along the LOS. Multiple populations of dust grains along the LOS
  could lead to an inaccurate estimate of $\beta$, especially in dense regions
  where the LOS is complicated. We quantify the emissivity as a function of
  density by fitting $\beta$ as a function of $A_V$ with a 1$^{\rm st}$-degree
  polynomial. We find $\beta$ = 1.33 ($A_V$ / 100\,mag) + 1.67 for California
  and $\beta$ = 1.16 ($A_V$ / 100\,mag) + 1.69 for Taurus. Perseus demonstrates
  unexpected behavior however. The dust emissivity decreases with $A_V$ as
  $\beta$ = -0.99 ($A_V$ / 100\,mag) + 1.72. Decreasing $\beta$ with $A_V$ is
  inconsistent with dust coagulation, but could be due to the inaccuracy of a
  single dust-grain population along the LOS.

  % See this post for the dust emissivity posts
  % https://ezbc.me/research/2016/03/14/dust-emissivity/


  The \htwosd\ maps reveal diffuse \htwo\ present across Taurus and California.
  In fact, diffuse \htwo\ comprises the majority of the mass of \htwo\ in each
  cloud. \citet{goldsmith08} examined $^{12}$CO and $^{13}$CO emission across
  Taurus and found evidence for diffuse molecular emission between and around
  major sub-regions. This study found that low-density gas is prevalent across
  Taurus and that half of Taurus' mass comes from material with $N(H_2)<2.1
  \times 10^{21}$ cm$^{-2}$. We find that 83\%, 70\% and 72\% of the \htwo\ mass
  in California, Perseus and Taurus resides in LOS with $N(H_2)<2.1 \times
  10^{21}$ cm$^{-2}$.

