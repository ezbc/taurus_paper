\subsection{Sample Selection with the Cold Clump Catalogue of Planck Objects}
\label{sec:sample_selection}

  \begin{figure*}[!ht]
    \begin{center}
    \includegraphics[width=\linewidth]{figures/multicloud_av_cores_map.pdf}

    \caption{\label{fig:core_regions} Location and regions of cores used in
      analysis. For each cloud, we adopt the ten clumps in the C3PO with the
      highest \nhtwo, totaling to 30 core regions in our entire sample
      \citep{planck11_23}. We adopt a wedge shape for each core region which we
      rotate by hand in order to include many diffuse, atomic-dominated LOS, and
      the dense, molecular-dominated LOS. The wide range of \hsd, traced by the
      dust column density will enable us to test \hi\-to-\htwo\ transition
      models. We assume that the regions can be described by a characteristic
      chemical/dust composition, CNM and WNM temperatures, and dynamics within a
      given core region.
    }

    \end{center}
  \end{figure*}

  We are interested in identifying regions within molecular clouds which can be
  described by characteristic property values. For example we will determine the
  characteristic WNM and CNM temperatures in each core. Identifying core regions
  is not trivial [e.g., \citealt{andersson91}] thus we take a simplified
  approach of selecting cold clumps and hand-selecting regions around the clumps
  as regions with characteristic ISM properties. 

  \citet{planck11_23} identified more than 10,000 cold sources which stand out
  against a warmer environment using Planck and IRAS data across the entire sky,
  posted as the {\em Cold Clump Catalogue of Planck Objects} (C3PO).  They
  identify sources by fitting photometry to color maps subtracted from a
  background identified within a 15\arcmin\ radius. With these identified cold
  residual clumps, they fit single modified black-body SEDs to the clump
  spectra.  These study calculated dust opacity and \nhtwo\ from the SEDs of
  each cloud. We use the \nhtwo\ measurements of the identified clumps to select
  our core region sample.

  We wish to test the presence of, and the properties of the \hi-to-\htwo\
  transition, thus we should select core regions with at least some \nhtwo\
  content. For each cloud, we adopt the ten clumps in the C3PO with the highest
  \nhtwo, totaling to 30 core regions in our entire sample.

  Next we pick the extent of the core regions by hand. We adopt a wedge shape
  for each core region in order to include many diffuse, atomic-dominated LOSs,
  and the dense, molecular-dominated LOSs. We rotate the wedges so that all core
  regions include independent LOSs, and do not include multiple dense clumps.
  See \autoref{fig:core_regions} for our chosen core regions. The cores we
  identified from the C3PO are similar to dense core regions outlined by
  \citet{lombardi10} and \citet{bally12}. We discuss some of the properties of
  these regions in \autoref{sec:core_environments}.

  The core regions we chose by hand each include the peak dust column density of
  the core region, and extend to much more diffuse dust column densities.  We
  will test \hi\-to-\htwo\ transition models with a wide range of \hsd, traced
  by the dust column density, see \autoref{sec:hi_htwo_properties} for a
  discussion of the \hi\ and \htwo\ properties of the regions. Each region
  contains 182 LOSs, corresponding to physical region sizes of 65\,pc$^{-2}$ for
  California, 29\,pc$^{-2}$ for Perseus, and 7\,pc$^{-2}$ for Taurus, assuming
  the distances in \autoref{table:cloud_summary} and 5\arcmin\ resolution.

\subsection{\hi\ and \htwo\ Properties}
\label{sec:hi_htwo_properties}

  The \hi\ properties across each cloud are mostly constant. Perseus and Taurus
  have median \nhi\ values of $\sim$\,8\,\colDens\ departing by no more than a
  factor 2 across the region. California has a higher median \nhi\ of
  $\sim$\,12\,\colDens. Our derived median \nhi\ value of 8\,\colDens\ is
  consistent with the average \nhi\ value derived by \citet{imara11} of
  $\sim$\,5\,\colDens. 
  %See \autoref{table:core_properties} for the estimated \hisd\ in each core
  %region. 
  The majority of \hisd\ thresholds within each cloud are within a
  factor of two from one another, with Taurus showing the most variation in
  \hisd\ across individual core regions.

  % http://ezbc.me/research/2015/08/19/bootstrapping/

  
  % cloud	    nhi_std	    nhi_median	nhi_mean	nhi_min	nhi_max
  % california	1.8	        12.2	    11.9	    7.3	    17.3
  % perseus	    1.4	        8.1	        8.1	        4.6	    13.7
  % taurus	    3.7	        8.2	        9.0	        3.2	    17.6

\subsection{Core Environments}
\label{sec:core_environments}

  Some evidence suggests cores in Taurus are quite dynamic. For instance the
  L1495\,/\,B213 filament extending over 8\,pc is thought to originate from a
  collision of two flows \citep{tafalla15}. L1535 powers an outflow of at least
  5.5\,pc in length \citep{bally12} and Herbig-Haro objects in L1551 drive a
  0.5\,pc outflow. B18 is one of most active regions in Taurus with at least a
  dozen young stars driving outflows \citep{bally12}.

  Only one internal cluster appears to be associated with California, NGC\,1579,
  embedded in the dark cloud of L1482. About 100 members reside in the cluster
  \citep{herbig04,andrews08}. The cluster is likely less than 2\,Myr old judged
  from its color-magnitude diagram \citep{lada09}. The higher radiation field of
  this region, described in \autoref{sec:rad_field} is consistent with an
  embedded cluster heating the surrounding ISM. \citet{li14} used CO-line
  surveys to suggest that L1482 is a filamentary coherent structure, which
  likely formed NGC\,1579. They also suggest that L1482 is in the process of
  dynamical evolution.

  Perseus has several dark regions, B5, B1E, B1, L1455, L1448, and L1451, and
  two star-forming regions, IC348 \citep{herbig98} and NGC1333 \citep{lada96}.
  See \citet{lee12} for a thorough description of the core regions in Perseus.
  \citet{lee12} found IC348 and NGC1333 to have excited dust temperatures
  relative to the other cores, consistent with internal star formation. This
  likely means an internal radiation field is present in the two core regions.





