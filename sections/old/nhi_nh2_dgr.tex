\subsection{Cloud Boundaries}
\label{sec:cloud_boundaries}

  \begin{figure*}[!th]
    \begin{center}
    \includegraphics[width=\linewidth]{figures/av_map.pdf}

    \caption{\label{fig:multicloud_map} \textit{Top:} Estimated \nhi\ map of
      field containing Taurus, California and Perseus. The \hi\ was integrated
      from -5 to 15 \kms, as found by \citet{lee12} for Perseus.
      \textit{Bottom:} Planck $A_V$ of the same field. The white outlines
      represent the boundaries of each region we chose. Our criteria for
      choosing cloud regions was to confine the clouds to regions where the
      background $A_V$ gradient did not vary greatly, and which included diffuse
      $A_V$ regions.
    }

    \end{center}
  \end{figure*}

While Perseus, California and Taurus belong to the Taurus-Auriga-Perseus complex
of dark and star-forming clouds, they are located at well-separated distances.
We therefore start by introducing rough GMC boundaries and in further analysis
treat each GMC as a separate entity.  In \autoref{fig:multicloud_map} images of
the whole complex (based on the coverage of the GALFA-HI survey) are shown in
\hi\ and $A_V$.  For illustration purposes only, we have integrated \hi\ over
the velocity range from $-5$ to 15 \kms, which \citet{lee12} found included
associated \hi\ emission to Perseus.  We overlay cloud boundaries used in our
study, which roughly follow the  regions identified in \citep{lombardi09}.  Our
main criteria are 1) confine the GMCs to regions where the background $A_V$ does
not vary significantly, 2) include as much of the diffuse $A_V$ as possible to
probe GMC outskirts.  In the case of Taurus, we include the diffuse filamentary
structures which appear to be associated with the cloud e.g., Taurus'
North-Western arm.

While \nhi\ varies only by a factor of 2 in \autoref{fig:multicloud_map}, the
$A_V$ image has a much higher dynamic range. A typical range in $A_V$ for each
one of three GMCs is from 0\,mag to 10's of mag.

\subsection{Determining \nhi\ for Individual GMCs}
\label{sec:nhi_coldens}

As \hi\ can include multiple velocity components along the line of sight, we
determine the velocity range of \hi\ emission that is likely associated with
each individual GMC. Similarly to \citet{imara11}, we fit multiple Gaussian
functions to the median \hi\ spectrum of each GMC. In the case that thermal and
turbulent broadening define \hi\ line profiles, a superposition of Gaussian
functions should reasonably represent \hi\ spectra.  The Taurus-Auriga-Perseus
molecular cloud complex is located below the Galactic plane, resulting in less
complex and confused \hi\ spectra relative to many other GMCs \citep{lee12}.  
  
  \begin{figure}[!ht]
    \begin{center}
    \includegraphics[width=\linewidth]{figures/multicloud_spectra.pdf}

    \caption{\label{fig:spectra} Median \hi\ spectra of each GMC (solid black
      line) and the median CO profile (the orange solid line). The dashed black
      lines are the Gaussian components fitted to the median \hi\ spectrum. The
      thicker black dashed line represents the components used to estimate the
      final \hi\ range of each GMC. The thick reddish-orange line is the fit to
      the median \hi\ spectrum. The width of the gray regions corresponds to our
      selected \hi\ velocity range used for further analysis.
      }

    \end{center}
  \end{figure}

\autoref{fig:spectra} shows the median \hi\ spectra of each GMC.  We fit as few
Gaussian functions as possible to accurately reproduce the spectra (shown as
dashed lines). In the case of Perseus and Taurus we use two Gaussian functions,
and California requires four functions. The median $^{12}$CO(1-0) spectrum is
shown in orange.  We assume that the Gaussian function that is nearest in
velocity to the CO peak is associated with the GMC, and  include all \hi\ within
$\pm 2\,\sigma_{\rm HI}$ of this Gaussian component in our \hi\ velocity range.
In the case of Perseus and Taurus  these selection criteria include most of the
velocity range occupied by the main \hi\ component (shown as gray region), while
the secondary very broad \hi\ component that extends to high negative velocities
is excluded as it does not have any corresponding CO emission.  However,
California is more complex, which is consistent with it being the closest to the
Galactic plane. It has two clear CO components \citep{kong15}, and the \hi\
profile is even more extended.  We include in our analysis two \hi\ Gaussian
components that are the closest in velocity to the two CO components. By
combining  the $\pm 2\,\sigma_{\rm HI}$ velocity range of those two \hi\
components we estimate the velocity range shown in \autoref{fig:spectra}.


To estimate the uncertainty in selected velocity range we consider two aspects.
First, we selected the velocity range by considering peaks in \hi\ and CO
emission which are usually not aligned in velocity. Turbulence can cause this
offset between \hi\ and CO emission. The offset corresponds to the minimum
uncertainty in the selected velocity range which we estimate as the absolute
difference between the peak velocities of the corresponding \hi\ and CO Gaussian
components. Second, by including \hi\ beyond the CO velocity extent we could be
overestimating the neutral gas associated with GMCs. Therefore, the maximum
uncertainty in the selected velocity range can be obtained as a difference
between our selected velocity range and the velocity range occupied only by CO
in the case of corresponding \hi\ and CO Gaussian component pairs. We finally
estimate the uncertainty in the \hi\ velocity range, $\delta_V$, as the average
between these minimum and maximum uncertainties.

\autoref{table:cloud_properties} lists the \hi\ velocity range for each GMC.  In
all cases, the velocity range is similar to the range found by \citet{lee12} and
\citet{imara11} for Perseus. This is encouraging considering that different
methods were used in these studies.  For example, \citet{lee12} estimated $-5$
to $+15$\,\kms, based on the spatial correlation between \nhi\ and $A_V$, while
\citet{imara11} estimated $-8$ to $+14$\,\kms\ based on the spatial variation of
\hi\ profiles and Gaussian fitting. We calculate \nhi\ by integrating over the
selected velocity range for each GMC under the optically-thin assumption.

  \begin{comment}

    Next, we compare the Gaussian-fit velocity range with selecting the
    relevant velocities of a cloud based on the standard deviation of the HI
    spectrum \citep{planck11_24}. The standard deviation spectrum of a region
    reveals structure more clearly than the median or mean spectra. We identify
    the \hi\ velocity range by hand, marking the edge of the range as the
    velocity where the spectrum falls to 0, or where two components in the
    spectrum meet. We use this method to show the various components within the
    three clouds in \autoref{fig:spectra}. The standard deviation spectrum does
    not reveal any structure in Perseus or Taurus not identified with the mean
    or median \hi\ spectrum. The standard deviation spectrum of California
    however shows three well-defined components. Since the median CO spectrum
    spectrum has two components, we include velocities including the two
    standard deviation components nearest the CO peak.

    The velocities chosen between the two methods are similar, see
    \autoref{table:hi_velocity_range}. However both are uncertain by at least a
    few \kms. The Gaussian-fitting is biased by the number of Gaussians we use
    to fit the spectrum, and examining the standard deviation spectrum, done by
    hand, is somewhat crude.  

  \end{comment}

\begin{comment}
  \begin{deluxetable}{lcccc}[!ht]
    \tabletypesize{\footnotesize}
    \tablecolumns{5}
    %\tablewidth{0pt}

    \tablecaption{\hi\ Velocity Range\label{table:hi_velocity_range}}

    \tablehead{\colhead{} & 
           \colhead{\hi\ Velocity\tablenotemark{a}} &
           \colhead{$\delta_V$\tablenotemark{b}} \\
           \colhead{} &
           \colhead{Range} &
           \colhead{} \\
           \colhead{} &
           \colhead{[\kms, \kms]} &
           \colhead{\kms} \\
         }

    \startdata{}\noindent

      \input{tables/multicloud_vel_ranges.tex}
    
    \enddata{} 
      
      \tablenotetext{a}{\hi\ velocity range associated with each cloud,
      as determined by Gaussian fitting of cloud median \hi\ spectrum
    \citep{imara11}.}

      \tablenotetext{b}{Uncertainty in \hi\ velocity range, defined by
      the absolute offset between the CO median spectrum peak and
    location of cloud Gaussian.}


    %\vspace{-0.8cm} \tablecomments{}
  \end{deluxetable}
\end{comment}

\subsection{DGR and Uncertainty}
\label{sec:determining_dgr}

  PLEASE CHECK FOR OBSOLETE SUB-SECTIONS.

  To derive \nhtwo\ from $A_V$ we need an estimate of the DGR.  We derive the
  DGR by performing a weighted least-squares fit to the relationship between
  $A_V$ and \nhi\ shown in \autoref{fig:av_vs_nhi}.  As we expect that \hi\
  traces the total gas column density in diffuse, primarily atomic regions, the
  fitted slope  $A_V$/\nhi\ is a good measure of DGR.

  As \autoref{fig:av_vs_nhi} shows, the vast majority of lines-of-sight appear
  to demonstrate a linear trend between $A_V$ and \nhi. The presence of \htwo\
  dominate the excursions from a linear relation at high $A_V$.  This figure
  also shows that while Perseus and California have a narrow range of \nhi\
  (variations by a factor of three at most), Taurus has a broader \nhi\
  distribution.  In addition, for Perseus and California, when \nhi\ reaches
  values of $\sim\,10\times 10^{20}$ cm$^{-2}$, the linear relation between
  $A_V$ and \nhi\ breaks due to the existence of molecular gas.  The situation
  appears more complex in the case of Taurus where high-$A_V$ excursions appear
  over a wide range of \nhi\ suggesting more significant spatial variations of
  physical properties and/or geometry/dynamics across this GMC.

  %I DO NOT THINK THIS SAYS MUCH OR IS NEEDED:
  %The DGR is much more uncertain than the variance of the fit to our single
  %  observations of each cloud. Systematic uncertainties of $A_V$ and \nhi\
  %  dominate the uncertainty of the DGR as discussed in
  %  Sections~\ref{sec:nhi_coldens} and~\ref{sec:dust_coldens}. Additionally we
  %  might expect that the regions we defined for each cloud in
  %  \autoref{sec:cloud_boundaries} might include lines-of-sight which may skew the
  %  cloud DGR from the true value, or that some lines-of-sight with molecular
  %  hydrogen present may skew the DGR. We thus implement a Monte Carlo simulation
  %  to mitigate such systematic bias and estimate the DGR uncertainty.


  %  \subsubsection{Monte Carlo Simulations}\label{sec:monte_carlo_simulations}
  We estimate the uncertainty on the DGR by following a similar method to
  \citet{leroy09} where we perform Monte Carlo simulations to calculate the DGR
  under various realizations of the data assuming uncertainties in both $A_V$
  and \nhi. In each simulation of the data we perform an
  orthogonal-distance-regression fit to the $A_V$-\nhi\ relationship. We use the
  variances of the $A_V$ and the \nhi\ for the weights of the fit. During this
  process,  we incorporate as many uncertainties (both systematic and random) in
  the data as possible by using the following steps. 

    \begin{enumerate}
      
      \item Add random normal noise to each pixel based the uncertainty of the
        $\tau_{353}$ image (\autoref{sec:data_planck_models}), as well as an
        estimate of the systematic uncertainty associated with the process of
        background-subtraction (see Section 2.5).  The combined (in quadrature)
        uncertainty for each pixel is then multiplied by 0.7 to incorporate an
        estimate of the uncertainty on the total-to-selective extinction
        (\autoref{sec:color_excess_to_av}).

      \item Derive \nhi\ by accounting for the uncertainty in the \hi\ velocity
        range $\delta_V$ (Table X). PLEASE SAY HOW IS THIS DONE BASED ON SOME
        RECENT CHANGES WE IMPLEMENTED.

      \item Rescale and background-subtract the {\em Planck} $A_V$ image. We
        divide the image by a uniformly-random number between 1 and the fitted
        slope between {\em Planck} and 2MASS $A_V$ (\autoref{sec:scaling_av}). 
%Dividing the {\em Planck} map by a
%        slope of one means to retain the original {\em Planck} $A_V$ values.
%        Dividing the {\em Planck} map by the slope between {\em Planck} and
%        2MASS means to bootstrap the {\em Planck} $A_V$ values entirely to the
%        2MASS scaling.

  We also subtract here the fitted intercept (Table 1). SAY HOW TO HANDLE
  UNCERTANTY IN THE INTERCEPT.

  % WE ARE NOT DOING THIS ANY MORE BUT ADDING THIS EARLIER IN THE PROCESS.
  %     \item Include the offset uncertainty by adding a normally-random
  %        offset of scale 0.2 mag
          (\autoref{sec:offsetting_planck_av}).

      \end{enumerate}

    We run 10,000 simulations. For each data realization we perform a
    weighted-least-squares linear fit without an intercept.  It is worth noting
    that the median error for data points above $A_V\sim5$\,mag is 1.5\,mag,
    while for $A_V<5$\,mag the median error is 0.5\,mag. Therefore, the low
    $A_V$ data points largely dominate the fit.  However, to ensure that DGR
    calculation is not affected by high-$A_V$ outliers, we follow the methods of
    \citet{chen15} and \citet{sandstrom13}, and  bootstrap the data with
    replacement, using the same number of lines-of-sight.  A FEW MORE WORDS (1-2
    SENTENCES) WOULD BE GOOD TO EXPLAIN BETTER WHAT EXACTLY IS BEING DONE IN THE
    bootstrap STEP.  We report the median and 68\% confidence interval on the
    DGR in \autoref{fig:av_vs_nhi}. We confirm that 10,000 simulations
    accurately measures the uncertainty in the DGR, we perform two more batches
    of 10,000 simulations. All derived DGR and uncertainties between the three
    batches are within 1\% of each other, assuring us 10,000 simulations
    accurately samples the uncertainties of the data.
    \autoref{table:cloud_properties} summarizes the Monte Carlo simulations
    results.  The distribution of the observed $A_V$ vs. \nhi\ and the fitted
    DGR are shown in \autoref{fig:av_vs_nhi}. 

      We find a similar DGR for Perseus, 12.3\,\dgrunit, with the DGR found in
      \citet{lee12} of 11\,\dgrunit. Within our DGR uncertainty our results
      agree with \citet{lee12}.  Our derived DGR for Taurus is a little less
      than two times higher than found by \citet{paradis12}, while for
      California we find DGR similar to the typical Galactic value $5.3\times
      10^{-22}$ mag cm$^2$ (e.g. Boglin et al. 1978). Our DGR for all three GMCs
      is within the range of DGRs found within the Galaxy using UV absorption
      measurements \citep{kim96}.

      \begin{figure}[!ht]
        \begin{center}
        \includegraphics[width=\linewidth]{figures/multicloud_av_vs_nhi.pdf}

        \caption{\label{fig:av_vs_nhi} $A_V$ vs. \nhi\ for each cloud. The
          contours include 99\%, 98\%, 95\%, 86\%, and 59\% of the data. The
          median bootstrapped fit using all data points is shown as a
          orange-dashed line.  The median values are shown as brown squares. Our
          estimate of the DGR is insensitive to outliers, i.e. the high-$A_V$
          pixels because of the bootstrap application.  The median error above
          $A_V$ of 5\,mag is 1.5\,mag, while below $A_V$ of 5\,mag the median
          error is 0.5\,mag. The uncertainty of \nhi\ is nearly constant for all
          data points around XXXXX.     
        }

        \end{center}
      \end{figure}
    
    \begin{deluxetable}{ l c c c }[!ht]
      \tabletypesize{\footnotesize}
      \tablecolumns{4}
      %\tablewidth{0pt}
      \tablecaption{Cloud Properties\label{table:cloud_properties}}

      \tablehead{\colhead{Property} & 
             \colhead{California} &
             \colhead{Perseus} &
             \colhead{Taurus}
           }

      \startdata{}\noindent

        \input{tables/cloud_properties.tex} 

      \enddata{}
      %\vspace{-0.8cm}

      \tablecomments{Median Dust-to-Gas Ratio (DGR) determined from from Monte
        Carlo simulations with 68\% confidence uncertainties. \hi\ range
        determined from fitting Gaussians to cloud median \hi\ spectrum
        \citep{imara11}.  $T_{\rm dust}$ average core region dust temperature
        measured from a modified SED fit from Planck IR maps along each LOS.
        $I_{UV}$ is the Lyman-Werner band (912\,-\,1108\,\angstrom) radiation
        field intensity relative to the Draine field, determined by assuming an
        average dust size and composition, and relating the dust temperature to
        the absorbed ambient radiation. $G_0^\prime$ is the same as $I_{UV}$
        except relative to the Habing field. Distance determined by stellar
        reddening photometry from \citet{schlafly14}.  Mass calculated from
        stellar extinction $A_k$ measurements \citep{lombardi10}. $N_{\rm YSO}$ is
        the young stellar object (YSO) count in the molecular cloud
        \citep{lada10}.  Star formation rate (SFR) calculated from the number of
        YSOs assuming a constant stellar age across the cloud \citep{lada10}.  
      }

    \end{deluxetable}


  \subsection{H$_2$}

  We assume a single DGR for the whole GMC and derive the H$_2$ column density
  using the relation:

  \begin{equation}\label{eq:av_dgr}
    A_V = {\rm DGR}\times [2 N({\rm H}_2) + N({\rm H}\,\textsc{i})].
  \end{equation}

  \noindent Ultra-violet absorption measurements of \htwo\ support this simple
  assumption \citep{rachford09}. In the case where the line-of-sight (LOS)
  contains no molecular hydrogen, the dust column simply traces the \hi\ column
  scaled by the DGR\@. A linear fit to the relationship between the dust column
  density and \nhi\ will give the DGR\@. The leftover dust column density
  unassociated with \nhi\ scales with \nhtwo\ by the DGR. 

  WHAT ABOUT RECENT DUVAL'S PAPER ON DGR CHANGING WITH PHASES? MAYBE MIN CAN ADD
  A FEW SENTENCES.

  \autoref{fig:hi_h2_maps} shows the \hisd\ and \htwosd\ maps for each cloud.

  \begin{figure*}[!ht]
    \begin{center}
    \includegraphics[width=\linewidth]{figures/hi_h2_maps.pdf}

    \caption{\label{fig:hi_h2_maps} \hisd\ (left) and \htwosd\ (right) for each
      cloud. We derived \hisd\ by integrating \hi\ over the velocity range shown
      in \autoref{table:cloud_properties}. We solved for \htwosd\ by
      bootstrapping Planck infrared $A_V$ to 2MASS stellar-extinction $A_V$, and
      applied the DGR presented in \autoref{table:cloud_properties} and \hisd\
      to \autoref{eq:av_dgr}.
    }

    \end{center}
  \end{figure*}


