
We adopt \hi\ and IR observations to trace \hisd\ and total hydrogen surface
density, \hsd, respectively. In this section we briefly describe the steps taken
by \citet{planck13_11} to derive color excess maps from the {\em Planck} IR maps
in \autoref{sec:data_planck_av}. We present the stellar-background extinction
maps from 2MASS in \autoref{sec:data_2mass} and describe our method to calibrate
and remove any background from the {\em Planck} color-excess maps using the
2MASS extinction in \autoref{sec:dust_coldens}. Finally we present CO data which
we use to compare to \hi\ spectra.

\subsection{Deriving $A_V$ with {\em Planck} Infrared Observations}
\label{sec:data_planck_av}

  Our analysis requires measurements of the diffuse dust columns densities in
  low-$A_V$ regions of molecular clouds. We use color excess maps derived
  from dust emission observed by {\em Planck} which have ten times the
  sensitivity of color excess maps than derived from optical stellar
  extinction \citep[e.g.][]{lada10}. We use several data products from
  \citet{planck13_11} which require a careful prescription to molecular
  clouds including diffuse and dense dust column densities. We describe in
  the following sub-sections their steps to model the dust emission as
  color-excess, and how we adopt their color-excess maps to model the dust
  column density.
  
  \subsubsection{All-sky Dust Models}

  \label{sec:data_planck_models}

    \begin{comment}
    \citet{planck11_24} used 3,000 GHz to 143 GHz imaging of Taurus to derive
    dust temperature maps. They found dust cooling on a pixel by pixel basis
    from 17.5 K to 13 K which they attribute to variations in either excitation
    conditions or optical properties. They found an increase of the measured
    dust optical depth at 250 $\mu$m per unit column density in the molecular
    phase by a factor of about 2 compared to the value found in the diffuse
    atomic ISM\@. The increase of optical depth per unit column density for the
    dust particles could be even higher in dense regions, owing to radiative
    transfer effects and the increase of selective to total extinction $R_V$ in
    dense regions conditions or optical properties.  
    \end{comment}

    \citet{planck13_11} derived an all-sky color-excess map from 857, 545, and
    353\,GHz \textit{Planck} data along with \textit{IRAS} 3000\,GHz data.
    Their first step towards deriving the dust color-excess maps was to fit a
    spectral energy distribution (SED) model of the dust emission across the
    four bands each convolved to the coarsest beamsize of 5\arcmin. The
    observed dust specific intensity is given by

    \begin{equation}\label{eq:dust_intensity}
        I_\nu = \tau_{\nu_0} B_\nu{(T)}
        {\left(\frac{\nu}{\nu_0}\right)}^{\beta}
    \end{equation}
   
    \noindent where $\nu$ is the frequency, $\nu_0$ is a reference frequency,
    $B_\nu{(T)}$ is the {\em Planck} function for dust at temperature $T$,
    $\beta$ is the spectral slope, and $\tau_{\nu_0}$ is the dust optical
    depth. The fitted dust optical depth should trace the dust mass column
    density, $M_{\rm dust}$, given a constant dust emissivity cross section,
    $\kappa_{\nu_0}$,

    \begin{equation}
        \tau_{\nu_0} = \kappa_{\nu_0} M_{\rm dust}.
    \end{equation}

    \citet{planck13_11} fit their all-sky maps to the dust specific intensity
    allowing $\tau_{\nu_0}$, $T$, and $\beta$ to vary for the four bands
    setting the reference frequency $\nu_0 =$ 353\,GHz. The zero-level of the
    {\em Planck} maps was set by \hi\ emission from 21-cm Leiden/Argentine/Bonn
    (LAB) data \citep{kalberla05} to avoid any monopole contribution of the
    cosmic infrared background (CIB). 

    One additional tracer of the dust column density is the dust radiance
    $\mathcal{R}$, or the dust integrated intensity,

    \begin{equation}
        \mathcal{R} = \int_\nu \tau_{353} B_\nu{(T)}
        {\left(\frac{\nu}{353}\right)}^{\beta} d\nu
    \end{equation}

    \noindent where $\tau_{353}$ represents the dust optical depth at
    353\,GHz. $\mathcal{R}$ represents the energy absorbed by the dust
    grains when the dust is in thermal equilibrium. For a constant
    radiation field strength $\mathcal{R}$ will trace the dust column
    density. %even in regions where $\sigma_{\rm e\,\nu}$ may vary.     

    The models of \citet{planck13_11} provide two tracers of dust column
    density, $\tau_{353}$ and $\mathcal{R}$. We discuss the caveats and
    benefits of using either tracer in the following section.
  
  \subsubsection{Dust Models Tracing Color Excess}
  \label{sec:planck_colorexcess}
  
    The authors take the final step to convert $\mathcal{R}$ and $\tau_{353}$
    to an estimate of dust column density by using background SDSS quasars to
    calibrate linear relationships with optical color excess $E(B-V)$. Both
    dust parameters demonstrate tight linear correlations with $E(B-V)$
    measurements from the quasar sightlines across the entire sky. 

    The color-excess measurements derived from $\tau_{353}$ and $\mathcal{R}$
    require careful application. At 30\arcmin\ resolution, variations by
    almost a factor of 10 are present in the dust opacity across the entire
    sky even in diffuse regions. The authors relate $\tau_{353}$ to the
    product of the dust emissivity, $\sigma_{353}$ and gas column density,
    \nH. Using the LAB 21-cm observations to estimate \nH, they found
    increasing $\sigma_{353}$ with increasing \nH. The increasing
    $\sigma_{353}$ with column density will lead to an overestimate of
    $E(B-V)$. Additionally, changes in the dust temperature cause changes in
    $\tau_{353}$ despite being separate parameters in the dust SED.\@

    $\mathcal{R}$ traces the radiation field strength, thus is a reliable
    tracer for diffuse, low column density regions with constant radiation
    fields, i.e., regions without internal sources of radiation.
    $\mathcal{R}$ becomes a less reliable tracer of column density in dense
    regions where the radiation field is likely attenuated. For the higher
    column density regions $\tau_{353}$ is a better tracer of the structure,
    but is still not well calibrated given the increasing $\sigma_{353}$ with
    \nH. Despite these shortcomings for molecular clouds, the radiance and
    optical depth color excess maps agree well with color excess measurements
    from 2MASS \citet{roy13} for the Taurus and $\rho$-Ophiucus clouds;
    Pearson correlation coefficients for Taurus were 0.86 with $\tau_{353}$
    and 0.75 with $\mathcal{R}$ against 2MASS color excess. 
    
    We use the color excess map derived from $\tau_{353}$ provided by
    \citet{planck13_11}. Despite the caveat that the dust opacity likely
    varies within a molecular cloud, the $\tau_{353}$ measure of color excess is
    still a more reliable tracer than $\mathcal{R}$. We address the
    uncertainty of $\tau_{353}$ as a tracer of dust column density in later
    analysis (\autoref{sec:dust_coldens}).

  \subsubsection{Color Excess to $A_V$}

  \label{sec:color_excess_to_av}

    The final step to derive a tracer of dust column density is to convert the
    color excess to extinction assuming a selective extinction curve.  We
    consider the line-of-sight measurements of UV background absorption
    measurements examined by \citet{rachford09} to be representative sample of
    the environments we see in Taurus, California and Perseus. Their sample
    covered a range of sightline environments across high galactic latitudes,
    with \ebv\ ranging from about 0.2\,mag to 1.0\,mag. 

    We adopt the mean value $R_V$ of the \citet{rachford09} sample and the
    standard deviation of the sample as the uncertainty on $R_V$, $R_V = 3.09
    \pm 0.7$. This agrees well with total-to-selective extinction $R_V\,=\,3.1$
    found for Perseus and California \citep{fitzpatrick99}.  \citet{whittet01}
    found variations in $R_V$ in Taurus using 54 reddened stars from 2MASS.\@
    However for regions of low extinction, $A_V < 3$\,mag, the selection
    extinction remained constant at $R_V$\,=\,2.97\,$\pm$\,0.15. Each of these
    independent derivations of $R_V$ agree well with each other.


  \subsubsection{Resolution}

  \label{sec:planck_resolution}
      
    The {\em Planck} archive maps come natively projected onto the HEALPix
    grid with 2048 facets \citep{gorski05}. We developed a code to
    transform a region of the sky from the HEALPix {\em Planck} projection
    to a Cartesian equatorial projection with pixels of constant
    arclength.\footnote{This extraction code, named \texttt{planckpy}, can
    be found at \url{https://bitbucket.org/ezbc/planckpy}.} We avoid
    over-sampling the {\em Planck} image in our analysis by using the
    \texttt{Miriad} \citet{sault95} task \texttt{regrid} to regrid the {\em
    Planck} maps onto pixels 5\arcmin\ to a side.

\subsection{2MASS $A_V$}
\label{sec:data_2mass}

  The {\em Two Micron All Sky Survey} (2MASS) allows us to compare the {\em
  Planck} survey to a robust measurement of the optical extinction. We obtained
  a 2MASS $A_V$ map from \citet{kainulainen09}, hereafter KBH+09, for each
  molecular cloud. The authors employed the \texttt{nicest} algorithm
  \citep{lombardi09} which compares the near-infrared colors of stars behind
  molecular clouds to a robust measurement of the optical extinction from a
  reference field near the cloud. This comparison yields estimates of a
  near-infrared extinction towards the stars in the molecular cloud region. We
  do not use 2MASS $A_V$ directly, instead we calibrate the {\em Planck} $A_V$
  with the 2MASS $A_V$ in \autoref{sec:2mass_calibration}.

  We also use the $A_V$ map from \citet{lee12} to estimate the uncertainties in
  background subtraction of the $A_V$ maps in
  \autoref{sec:background_uncertainty}. \citet{lee12} adopted a separate 2MASS
  $A_V$ map from \citet{ridge06}, hereafter R+06. R+06 used a similar derivation
  method to that of KBH+09 to derive $A_V$, however chose different background
  reference fields to calibrate the Perseus $A_V$.

  Both maps have an uncertainty of about 0.2\,mag. This uncertainty arises from
  photometric errors on the background stars, as well as varying background star
  counts at each position.

  The R+06 2MASS $A_V$ map has a resolution of $5\farcm$, thus is directly
  comparable with the {\em Planck} map. The KBH+09 $A_V$ map has a resolution of
  $2\farcm4$, which we smooth with a Gaussian kernel to $5\farcm$ resolution.
  We finally regrid both 2MASS $A_V$ maps onto the {\em Planck} $A_V$ map grid.

  \begin{comment}
  \subsubsection{Calibration}
  \label{sec:2mass_calibration}
  
    We will calibrate the {\em Planck} IR $A_V$ map to the 2MASS $A_V$ map of
    \citet{kainulainen09} by both a scalar offset and a linear coefficient. Our
    method is similar to \citet{lee12} who scaled their IR $A_V$ map by
    bootstrapping the IR $A_V$ to their 2MASS $A_V$ map, but offset their IR map
    by correlating the dust optical depth with \nhi. We will use the 2MASS $A_V$
    map to bootstrap the IR $A_V$ as well as offset the IR $A_V$ to account for
    any background.
    
    However we would like to quantify the uncertainty in this calibration,
    since the 2MASS $A_V$ maps include biases of their own. We compare the 2MASS
    $A_V$ map from \citet{kainulainen09} with the 2MASS $A_V$ from
    \citet{ridge06} to estimate the calibration uncertainties.
    
    Both authors employed the \texttt{nicest} algorithm \citep{lombardi01}
    which compares the near-infrared colors of stars behind molecular clouds
    with control fields, free from extinction associated with the molecular
    cloud. This comparison yields estimates of a near-infrared extinction
    towards the stars in the molecular cloud region.

    The 2MASS $A_V$ maps require a reference region for calibration. In the
    case of the \citet{kainulainen09} 2MASS $A_V$ map (hereafter KBH+09 $A_V$),
    the $A_V$ was calibrated by a 2D interpolation across several background
    regions. Both the \citet{ridge06} (hereafter R+06) and KBH+09 maps are
    background subtracted. We use these maps later to calibrate the {\em
    Planck} $A_V$ maps, excluding excess $A_V$ in
    \autoref{sec:dust_coldens}.

  \end{comment}

\subsection{Calibrating {\em Planck} Dust Column Density}
\label{sec:dust_coldens}
  
  The IR-derived {\em Planck} color excess measurements show remarkable
  agreement with the stellar-extinction-derived 2MASS color excess across the
  sky despite the vastly different derivation methods. For example in Taurus,
  the correlation coefficient between the 2MASS and {\em Planck} color excess
  is 0.86 \citep{planck11_24}. However some discrepancy between the two color
  excess measurements require a careful application of the {\em Planck}
  data.{\em Planck} \ebv\ tends to show systematically higher \ebv\ than 2MASS
  (see Figure 26 of \citealt{planck11_24}). Many factors could lead to this
  discrepancy, for IR-derived color excess, factors include changes in grain
  composition, multiple dust temperatures along the line of sight, or changing
  dust emissivity. For stellar-background-derived color excess the factors
  include changing stellar background counts and foreground stars.
  
  Previous studies have calibrated {\em Planck} extinction to stellar
  extinction measurements based on disagreements between the {\em Planck} $A_V$
  and the 2MASS stellar-extinction $A_V$. Diffuse emission from dust
  contributes to all three clouds which is likely unassociated with each
  cloud.  This diffuse emission could be due to several factors including
  foreground / background extincting dust unassociated with a cloud or cosmic
  infrared background (CIB) emission. The 2MASS $A_V$ measures the extinction
  of background stars relative to background fields, thus is inHerently
  background-subtracted. \citet{lee12} bootstrapped their IR-derived $A_V$ to
  2MASS $A_V$ by fitting a linear relationship between the two and removing the
  intercept from the IR-derived $A_V$. \citet{zari15} calibrated IR K-band
  stellar extinction ($A_K$) to 2MASS $A_K$ assuming a linear relationship
  between the two, scaling the {\em Planck} $A_K$ to the 2MASS $A_K$, and
  removing a background.  \citet{lombardi15} used a combination of IR emission
  and stellar extinction to determine a background of $A_K \sim 0.1$ mag is
  present in California and $A_K \sim 0.01$ for Taurus and Perseus, $A_V \sim$
  0.9 and 0.1\,mag respectively. 

  \subsubsection{Scaling Planck IR $A_V$}
  \label{sec:scaling_av}

    The absolute dust column density of a cloud is not well-defined for dense
    molecular clouds; we estimate the {\em Planck} and 2MASS $A_V$ to bound the
    true $A_V$. \citet{planck15_29} found {\em Planck} IR-derived dust
    color-excess to scale with 2MASS dust color-excess by at least a factor of
    2. To remove potential dust unassociated with a cloud, and the discrepancy
    between IR- and stellar-extinction measurements of color-excess, we scale
    the {\em Planck} IR $A_V$ and remove a background.  However the 2MASS $A_V$
    likely saturates for $A_V > 10$\,mag due to uncertain background stellar
    counts. We thus perform a simple linear fit between the {\em Planck} $A_V$
    and 2MASS $A_V$ data for 2MASS $A_V < 10$\,mag.  The slopes and intercepts
    of the fits are shown in \autoref{table:planck_2mass}. The intercepts
    derived for California and Taurus are similar to that of \citet{lombardi15},
    however Perseus shows the {\em Planck} $A_V$ underestimates the background
    $A_V$ by $-0.4$\,mag.

    We bootstrap the {\em Planck} $A_V$ to the 2MASS $A_V$ in all subsequent
    analysis in two ways:

    \begin{enumerate}

      \item  We assume that the 2MASS $A_V$ is background-subtracted. We
        subtract the fitted intercept in \autoref{table:planck_2mass} from the
        {\em Planck} $A_V$ in all further analysis.

      \item We assume that the true $A_V$ lies between the {\em Planck} and the
        2MASS $A_V$, where {\em Planck} $A_V$ is the maximum possible $A_V$ and
        2MASS is the minimum $A_V$. We scale the {\em Planck} $A_V$ by 1/2 of
        the fitted {\em Planck} - 2MASS $A_V$ slope in
        \autoref{table:planck_2mass} for all further analysis, e.g., we divide
        the {\em Planck} $A_V$ in Perseus by 1.15.

    \end{enumerate}

    \begin{deluxetable}{lcc}[!ht]
      \tabletypesize{\footnotesize}
      \tablecolumns{4}
      %\tablewidth{0pt}

      \tablecaption{{\em Planck} vs. 2MASS $A_V$
      Relationship\label{table:planck_2mass}}

      \tablehead{\colhead{} & 
             \colhead{Slope} &
             \colhead{Intercept} \\ 
             \colhead{} &
             \colhead{[{\em Planck} $A_V$ / 2MASS $A_V$]} &
             \colhead{[mag]} \\
           }

      \startdata{}\noindent

      Perseus  & 1.31\,$\pm$\,0.04 & -0.40\,$\pm$\,0.04\\ 
      California & 1.43\,$\pm$\,0.01 & 0.53\,$\pm$\,0.02\\ 
      Taurus   & 1.46\,$\pm$\,0.01 & -0.03\,$\pm$\,0.02\\ 
      
      \enddata{}
      %\vspace{-0.8cm}
      %\tablecomments{}
    \end{deluxetable}

    The fitted uncertainties in the intercept between the {\em Planck} and
    2MASS $A_V$ relationship likely do not encompass all uncertainties present
    in the background subtraction. We thus compare two different 2MASS $A_V$
    maps of Perseus in \autoref{sec:background_uncertainty} to estimate the
    systematic uncertainties associated with deriving stellar-background $A_V$.

  \subsubsection{Estimating $A_V$ Background Uncertainty}
  \label{sec:background_uncertainty}

    Systematics likely dominate the uncertainty in the background-subtraction
    of the 2MASS KBH+09 $A_V$ used to scale the {\em Planck} $A_V$.  The
    systematics include the choice of background reference fields, and the
    interpolation function for the background. One way to estimate the
    uncertainty of the background is to examine the relationship between the
    KBH+09 2MASS $A_V$ and the R+06 2MASS $A_V$. 
    
    R+06 background-subtracted their $A_V$ map independently from the KBH+09
    data.  Thus an estimate of the error of the intercept for the Planck
    data is to use the fitted intercept between the KBH+09 and R+06
    relationship. The calculated intercept of fitting a first-degree
    polynomial between the R+06 and KBH+09 $A_V$ data is 0.2\,mag. We adopt
    this difference in offsets between the two stellar-background $A_V$
    maps as the systematic uncertainty of the background-subtraction. We
    add the systematic uncertainty in quadrature to the fitted intercept
    uncertainty between the {\em Planck} and KBH+09 $A_V$ to get the total
    uncertainty in the background-subtraction of the {\em Planck} $A_V$. For
    example, the total uncertainty of the background-subtraction,
    $\sigma_B$, of Perseus is $\sqrt{0.04^2 + 0.2^2} = 0.204$\,mag.

    %\end{comment}

\subsection{GALFA \hi}
\label{sec:data_galfa_hi}

  %\begin{comment}
  Our \hi\ data comes from the {\em Galactic Arecibo L-band Feed Array}
  (GALFA)-\hi\ survey. The seven-pixel receiver at Arecibo yields an effective
  beam size of $3\farcm9 \times 4\farcm1$ \citep{peek11}. The GALFA-\hi\
  spectrometer, GALSPECT, has a velocity resolution of 0.184 \kms\ and covers
  -700 \kms\ to +700 \kms\ in the local standard of rest (LSR) frame. The RMS
  noise in a 1\,\kms\ channel ranges from 140\,mK to 60\,mK, with a median of
  80\,mK.

  For direct comparison between {\em Planck} $A_V$ and GALFA-\hi\ observations
  we require them to have the same angular resolution. We take the geometric
  mean of the GALFA-\hi\ observations, $\sqrt{3\farcm9 \times 4\farcm1} =
  3\farcm95$, then smoothed the GALFA-\hi\ data using \texttt{Miriad} task
  \texttt{smooth} to 5\arcmin\ with a Gaussian kernel, full-width half-maximum
  (FWHM) of $3\farcm07$ and integrated area of 1. We regridded the \hi\ spatial
  axes to the {\em Planck} image with 5\arcmin\ pixel lengths.  We also convolve
  the \hi\ data with a Gaussian kernel to match the CO resolution of $8\farcm4$
  (\autoref{sec:data_cfa}), in order to compare the global spectra of each cloud
  (\autoref{sec:nhi_coldens}).
  %\end{comment}


\subsection{CfA $^{12}$CO J\,1$\rightarrow$0}
\label{sec:data_cfa}

  We use the $^{12}$CO spectra from \citet{dame01} obtained by the 1.2\,m
  telescope at the Harvard-Smithsonian Center for Astrophysics (CfA). The survey
  includes observations of the Galactic plane and local molecular clouds
  including California, Taurus, and Perseus. The data have an angular resolution
  of $8\farcm4$ and an RMS noise of 0.25\,K  per 0.65\,\kms\ -wide channel. When
  we compare the median spectra of the CO data to the \hi\ data in each cloud
  (\autoref{sec:nhi_coldens}), we smooth the \hi\ data to the CO resolution.





