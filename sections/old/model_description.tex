
In this section we outline the two steady-state models predicting \hisd\ and
\htwosd\ for individual core regions, and the application of the models to
observations of individual core regions. Both models, \citet[][K+09]{krumholz09}
and \citet[][S+14]{sternberg14}, predict an \htwo\ / \hi\ ratio, \rhtwo, for a
single core as a function of total gas surface density, \hsd:

  \begin{equation}
    R_{\rm H_2} = 
                  \frac{\Sigma_{\rm H}}{\Sigma_{\rm H\,\textsc{i}}} - 1 = 
                  \frac{\Sigma_{\rm H_2}}{\Sigma_{\rm H\,\textsc{i}}}
  \end{equation}

\noindent where \htwosd\ and \hisd\ are observables. Another interpretation is
both models predict \hisd\ given \hsd. The prediction of \rhtwo\ in both models
is for a single core with an atomic envelope and a molecular core. Our
observations of \htwosd\ and \hisd\ will enable us to compare \rhtwo\ as a
function of \hsd\ for many individual cores possessing varying \rhtwo\ with the
model predictions. 

\subsection{Krumholz et al. (2009) Model}
\label{sec:krumholz_model}

  K+09 derived their column density calculations from the analytic model of
  \citet[][hereafter K+08]{krumholz08} of \htwo\ formation and photodissociation
  in a spherical cloud bathed in a uniform ISRF.\@ The K+08 and K+09 models
  predicts the \hi-to-\htwo\ transition from a competition between
  \hi-associated dust and \htwo-line absorption. See Section 6 of \citet{lee12}
  for a summary of the results. Here we quickly summarize the important
  assumptions and results.

  \cite{krumholz08} derived the location of an infinitely-sharp \hitohtwo\
  transition based on two important quantities: the radiation field, $\chi$ and
  the dust optical depth $\tau_r$. $\chi$ is a dimensionless quantity described
  as the ratio of the rate at which Lyman-Werner (LW) photons are absorbed by
  dust grains to the rate at which they are absorbed by \htwo. $\tau_r$ is the
  optical depth of the dust if the dust volume density were that of the \ncnm.
  Assuming the dust traces the gas content, \citet{krumholz08} derived an \hi\
  surface density necessary for \htwo\ formation as a function of the \hi\
  optical depth.

  K+09 extended the model to relate $\chi$ to the CNM properties. They began by
  describing the the CNM number density, \ncnm, as a scalar, \phicnm, times the
  minimum CNM number density required for the CNM to remain in pressure
  equilibrium with the WNM
  
  \begin{equation}\label{eq:phi_cnm}
    \phi_{\rm CNM} = \frac{n_{\rm CNM}}{n_{\min}}
  \end{equation}

  \noindent \citep{wolfire03}. K+09 wrote their dimensionless radiation field
  measure $\chi$ to \phicnm\ as  

  \begin{equation}\label{eq:chi}
    \chi = 2.3 \frac{1 + 3.1 Z^{\prime\ 0.365}}{\phi_{\rm CNM}}.
  \end{equation}

  \noindent where $Z^\prime$ is the gas-phase metallicity. Given the transition
  profile derived by \citet{krumholz08}, K+09 were then able to relate the ratio
  of \htwosd\,/\,\hisd\,=\,\rhtwo\ as
  
  \begin{equation}
    R_{\rm H_2} = \frac{4 \tau_c}{3 \psi} \left[1 + 
      \frac{0.8 \psi \phi_{\rm mol}}{4 \tau_c + 3(\phi_{\rm mol} -
    1)\psi}\right] - 1
  \end{equation}

  \noindent where $\tau_c$ is the dust optical depth of the cloud if \hi\ and
  \htwo\ dust is well-mixed, $\psi$ is a dimensionless radiation field, and
  $\phi_{\rm mol}$ is the ratio of number densities between the cloud molecular
  component and CNM component. $\tau_c$ is a function of \ncnm\ since dust
  content is proportional to gas content, and the size of the cloud. Therefore
  $\tau_c$ is a function of the \hisd. They modeled $\psi$ using empirical
  results from PDR models as a function of the ratio of the rate at which
  Lyman-Werner (LW) photons are absorbed by dust grains to the rate at which LW
  photons are absorbed by \htwo, $\chi$. \rhtwo\ is weakly dependent of the ISRF
  in this model.  
  
  For MW-type environments, K+09 predicted a \hisd\ threshold of 10\,\msunpc\
  for solar metallicity. This is due largely to the limited range of \ncnm, and
  thereby $\tau_c$, values permitted by cooling and heating of hydrogen gas. In
  this model, if \phicnm\ is less than 1 or greater than 10, the CNM and WNM are
  no longer in pressure balance, breaking the fundamental assumptions of the
  model. For \phicnm$ < 1$ the CNM will be too dense to remain stable. For
  \phicnm$ \gg 10$, the WNM cannot provide a high enough pressure and will
  become ionized.

  The most uncertain parameter for a molecular core is \phicnm, thus for
  multiple observations of \rhtwo\ as a function of \hsd\ in a core region
  containing many cores with similar properties with varying molecular
  fractions, \phicnm\ can be constrained. In other words, \rhtwo\ is a function
  of both \phicnm\ and \hsd. We are able to observe \rhtwo\ and \hsd\ for
  individual cores. For a collection of similar cores, i.e. a core region, we
  are able to constrain the \phicnm\ parameter.

  We are also able to extract the predicted \hisd\ value at the \hitohtwo\
  transition given a model fit to the \rhtwo\ profile of a core region. We
  consider the \hitohtwo\ transition to be when \hisd$\,=\,$\htwosd, or rather
  when \rhtwo$\,= 1$.  Since the K+09 model predicts an exponential increase in
  the \htwosd\ after the necessary \hisd, the \hisd\ at the \hitohtwo\
  transition is a threshold / maximum in the \hisd. We interpret the transition
  \hisd\ as the \hisd\ threshold.

  We emphasize that the K+09 model assumes steady-state pressure equilibrium
  between the CNM and WNM. With this assumption we can predict \ncnm\ and the
  CNM kinetic temperature, \tcnm. First, we can calculate \ncnm\ from
  \autoref{eq:phi_cnm} after measuring \phicnm\ for a particular core region. We
  can then assume a value for $n_{\rm min}$ determined by cooling and heating
  functions of neutral gas as a function of the radiation field in Habing units,
  $G_0^\prime$, and dust-phase metallicity \citep{wolfire03} as
  
  \begin{equation}\label{eq:calc_ncnm}
    n_{\rm min} = \frac{31 G_0^\prime}{1 + 3.1 Z^{\prime\,0.365}}
  \end{equation}
  
  \noindent in units of cm$^{-3}$. Finally, we calculate \tcnm\ assuming a
  constant Galactic pressure of $P/k\,\sim\,3,700$\,K \citep{jenkins11} as

  \begin{equation}\label{eq:t_cnm}
    T_{\rm CNM} = \frac{P/k}{n_{\rm CNM}}
  \end{equation}

  \noindent where $k$ is the Boltzmann constant.

\subsection{Sternberg et al. (2014) Model}
\label{sec:sternberg_model}

  The S+14 model predicts the \hi-to-\htwo\ transition from a three-way
  competition between \hi-associated dust, \htwo-associated dust, and \htwo-line
  absorption. The inclusion of \htwo-associated dust separates the S+14 from the
  K+09 model. S+14 predict an \hisd\ threshold given by 

  \begin{equation}\label{eq:s+14_hisd}
    \Sigma_{\rm H\,\textsc{i}} = \frac{6.71}{Z^\prime \phi_g}
      \log\left[\frac{\alpha G}{3.2} +
    1 \right] \,\, [{\rm M_\odot pc^{-2}}]
  \end{equation}

  \noindent where $Z^\prime$ is the metallicity relative to solar, $\phi_g$ is a
  scalar of order unity of the dust-absorption cross-section dependent on the
  dust population.  $\alpha$ is the ratio of free space dissociation rate to the
  \htwo\ formation rate. $G$ is the cloud-averaged self-shielding factor, a
  measure of the dust-absorption efficiency of the \htwo-dissociating photons.
  $G$ depends on the competition of \htwo\ line absorption and \htwo\ dust
  absorption. We neglect the contribution of Helium in the gas density.

  The \hi-to-\htwo\ transition profiles and column densities are controlled by
  the dimensionless parameter $\alpha G$. Physically, \aG\ is the ratio of the
  dust associated with \hi\ (described as \hi-dust) absorption rate of the
  effective unattenuated \htwo\ dissociation flux to the \htwo\ formation rate.
  $\alpha G$ determines the LW optical depth due to \hi-dust. The \hisd\
  threshold increases with increasing $\alpha G$. By relating the dust optical
  depth with the dust properties and environment, they derive

  % alphaG(n, I_UV, phi_g, Z_g)
  \begin{equation}\label{eq:alphaG}
    \alpha G = 1.54 \frac{I_{\rm UV}}{(n/100\,{\rm cm}^{-3})}
            \frac{\phi_g}{1 + (2.64\phi_g Z^\prime)^{1/2}}
  \end{equation}

  \noindent where $I_{\rm UV}$ is the free-space FUV intensity relative to the
  Draine Field, $n$ is the total hydrogen gas volume density (cm$^{-3}$), and
  $Z^\prime$ is the gas-phase / dust metallicity relative to solar.  $\alpha G$
  determines \nhi\ and the shape of the \hi-to-\htwo\ transition.  When $I_{\rm
  UV}$ is low, the weak-field limit, $\alpha G \ll 1$, the \hi-to-\htwo\
  transition is gradual. The atomic column is mostly present in the molecular
  portion of the core. In the strong field limit, $\alpha G \gg 1$, the
  \hi-to-\htwo\ transition is sharp, leading to a threshold in \hisd.  The
  \hi-associated dust dominates the absorption of FUV photons in the
  strong-field limit.

  In a similar vain to the K+09 model, the most uncertain parameter for a
  molecular core is \aG. The S+14 model predicts \hisd\ as a function of \aG.
  Since \rhtwo$ = $\hsd\,/\,\hisd - 1, the predicted \rhtwo\ is a function of an
  observed \hsd\ = \hisd\ + \htwosd\ and \aG. We are able to constrain \aG\ in
  the same way we do for \phicnm\ in the K+09 model: fit the S+14 model to
  multiple observations of \rhtwo\ as a function of observed \hsd\ in a core
  region containing many cores. The cores in the core region we assume have
  similar properties relevant to the S+14, e.g., dust metallicity, but have
  varying molecular fractions. 

  We are able to extract the predicted \hisd\ value at the \hitohtwo\ transition
  given a model fit to the \rhtwo\ profile of a core region in the same way as
  described in \autoref{sec:krumholz_model}. However the S+14 model does not
  assume an infinitely-sharp \hitohtwo\ transition as the K+09 model does.  In
  the weak radiation-field limit, the S+14 model predicts a steadily increasing
  \hisd\ with \htwosd. In the strong radiation-field limit the S+14 model
  predicts a similar sharp \hitohtwo\ transition as the K+09 model predicts.
  Only in the strong radiation-field limit will we interpret the transition
  \hisd\ as the \hisd\ threshold for the S+14 model.

  $n$ (S+14 model) and \ncnm\ (K+09 model) are distinctly different, thus the
  models have potentially vastly different interpretations. See for example the
  comparison between \citet{lee12} who find the K+09 model describes the
  \hitohtwo\ transition well with the CNM as the shielding envelope, and
  \citet{bialy15} who found the S+14 model applied to the same data predicts the
  shielding \hi\ envelope consists of a multi-phased neutral medium. We predict
  $n$ by fitting \aG\ to the \hi\ and \htwo\ distributions in individual core
  regions, then solving for $n$ in \autoref{eq:alphaG}. We are then able to
  predict a characteristic total hydrogen temperature $T$ by a typical pressure
  between the WNM and CNM in the same was as in \autoref{eq:t_cnm}.

  Finally, \citet{bialy16} extended the S+14 model to consider the total gas
  surface density required to form stars.

  \input{sections/core_figures.tex}

\subsection{Model Application}

  For the K+09 and S+14 models we fit \rhtwo\ as a function of \hsd.   We apply the K+09 model to all three clouds in the same manner as
  \citet{lee12} did for Perseus.   We fit for \phicnm\ and hold all other
  variables constant. In particular we set $\phi_{\rm mol}$ = 10 and $Z^\prime =
  Z_\odot$ (see later in section for justification of metallicity).  We
  emphasize that the \phicnm\ largely determines \rhtwo, while \rhtwo\ has
  secondary dependencies on $Z^\prime$, $\phi_{\rm mol}$, and the incident ISRF.
  The \hisd\ is inversely related to \phicnm\ in the K+09 model.

  For a given core region we fit for \aG\ in the S+14 model. We do however
  account for potentially-varying dust properties in the S+14 model. Typically
  the galactic value of $\phi_g$ is of order unity, but \citet{lee12} found a
  DGR about two times higher than galactic, thus the dust properties may differ
  from galactic.  The dust absorption cross section, $\sigma_g$, given by 

  \begin{equation}
    \sigma_g = 1.9\,\times\,10^{-21}\,\phi_g Z_g\,{\rm cm}^{-2}
  \end{equation}

  \noindent in the S+14 model determines the behavior of \hi-to-\htwo\
  transition as well as the predicted average gas density (see
  \autoref{eq:alphaG}). For a galactic DGR / dust opacity, $\phi_g$ = 1 and $Z_g
  = 1\,Z_\odot$. 

  A few measurements of stellar metallicities exist for Taurus and Perseus, see
  \autoref{table:cloud_properties}. The metallicities were determined from
  spectra of an A3 star by \citet{gonzalez09} for Perseus and T Tauri stars by
  \citet{dorazi11} for Taurus. Since the three clouds in our sample are
  relatively quiescent without any \hii\ regions, we are unable to get gas-phase
  metallicity measurements. We assume that the stellar metallicities measured
  for Perseus and Taurus are similar to the gas-phase metallicity. We
  extrapolate California's metallicity to be solar as well.

  We choose to vary $\sigma_g$ with the DGR in each Monte Carlo simulation,
  since the dust-absorption cross section scales with the DGR / dust opacity.
  We assume that a single cloud metallicity describes the dust-phase metallicity
  of each core region. The low star-formation rates in each cloud do not offer
  reason to believe the metallicity to vary across core regions. Since each
  Monte Carlo simulation leads to a different empirical DGR , we set $\phi_g$
  equal to the scalar of the derived DGR / Galactic DGR, the Galactic DGR being
  where $\phi_g$ = 1 and $Z^\prime = 1$\,\zsun. For each cloud $\phi_g$ is,
  Taurus: $\phi_g = 1.82^{+0.48}_{-0.46}$, California: $\phi_g =
  1.32^{+0.43}_{-0.43}$, Perseus: $\phi_g = 2.40^{+0.55}_{-0.49}$.

  S+14 considered two cases of irradiation geometry of an optically-thick slab
  of \htwo\,/\,\hi, isotropic and beamed. For a sharp \hi-to-\htwo\ transition,
  the isotropic radiation leads to a spherical geometry with a molecular core
  and atomic shell, as in K+09. We thus assume the isotropic irradiation
  geometry in our application of the S+14 model in order to be better compared
  with the K+09 model.

  We calculate our final \hisd\ by integrating \hi\ for each cloud across the
  \hi\ velocity range specified in \autoref{table:cloud_properties}. Deriving
  \nhtwo\ requires using $A_V$ scaled by the DGR to estimate the total gas
  column density in \autoref{eq:av_dgr}. The final $A_V$ we use is the Planck
  $A_V$ bootstrapped to the 2MASS $A_V$. This includes subtracting an intercept
  from and scaling the Planck $A_V$ for each cloud, outlined in
  \autoref{sec:dust_coldens}. As an example, for Perseus, we calculate \nhi\ by
  integrating \hi\ from -8 to 13\,\kms\ (\autoref{table:cloud_properties}). We
  use the final $A_V$ as (Planck $A_V$ + 0.4\,mag) / 1.15
  (\autoref{table:planck_2mass}), scale the final $A_V$ by a DGR of
  11.0\,\dgrunit\ (\autoref{eq:av_dgr}), and subtract \nhi. We finally convert
  \nhi\ and \nhtwo\ to \hisd\ and \htwosd\ with \autoref{eq:nhi_to_hisd}.

  In Figures~\ref{fig:california_hi_vs_h}, ~\ref{fig:perseus_hi_vs_h},
  and~\ref{fig:taurus_hi_vs_h} we show our observables, \hisd\ as a function of
  \hsd, and the corresponding S+14 and K+09 model fits. We also show in
  Figures~\ref{fig:california_rh2_vs_h},~\ref{fig:perseus_rh2_vs_h},
  and~\ref{fig:taurus_rh2_vs_h} \rhtwo\ as a function of \hsd, or rather the
  \hitohtwo\ transition profiles. We fit only the $\alpha G$ and $\phi_{\rm
  CNM}$ parameters in the S+14 and K+09 models, respectively. We discuss the
  results of the \hitohtwo\ transition profiles in the next section,
  \autoref{sec:discussion}.

  We find that diffuse atomic gas is pervasive within the majority of cores.
  Some XX\% of LOS have \rhtwo$ < 1$. 

  The spread in the \hisd\ at the \hitohtwo\ transition, defined as the \hisd\
  when \rhtwo$ = 1$, is tight for California and Perseus, but varies hugely for
  Taurus. The transition \hisd\ is between 9\,\msunpc\ and 11\,\msunpc\ for
  California, and between 6\,\msunpc\ and 7\,\msunpc\ for Perseus. However the
  transition \hisd\ ranges from 3\,\msunpc\ to 9\,\msunpc\ in Taurus.





  \begin{comment}

  \subsection{Error from Region Selection}

    We use the isolated core G160.49-16.81 as an test subject for our systematic
    uncertainty in region selection. We perform the same monte carlo simulation
    outlined in \autoref{sec:monte_carlo_simulations}, except that we
    randomly rotate the core region about the core position. We find that the
    systematic uncertianties in region selection have similar value as the other
    factors accounted for in \autoref{sec:nhi_nH2_dgr}.

  cloud,core,ra,dec,phi_cnm,phi_cnm_error_low,phi_cnm_error_high,alphaG,alphaG_error_low,alphaG_error_high,hi_trans,hi_trans_error_low,hi_trans_error_high


  perseus,G160.49-16.81 
  \phicnm\ = 8.39^{+1.07}_{-0.63}
  \aG\ = 2.93^{+0.41}_{-0.34}

  perseus,G160.49-16.81,56.899969479093535,32.916064344285694,8.392737089069533,0.6321478768962665,1.0789649380479869,2.933600840134079,0.33645555289518114,0.41655896816680027,,,

  without rotation:
  \phicnm\ = 9.73^{+0.75}_{-0.36}
  \aG\ = 2.91^{+0.10}_{-0.29}

  \end{comment}



