
Our ultimate goal is to compare the relationship between \nhtwo\ and \nhi, thus
a careful derivation of \nhtwo\ is necessary. We derive \nhtwo\ by a simple
scaling between dust and gas column densities. We first assume a molecular cloud
has a single Dust-to-Gas Ratio (DGR) where

\begin{equation}\label{eq:av_dgr}
  A_V = {\rm DGR}\times [2 N({\rm H}_2) + N({\rm H}\,\textsc{i})].
\end{equation}

\noindent Ultra-violet absorption measurements of \htwo\ support this simple
assumption \citep{rachford09}. In the case where the line-of-sight (LOS)
contains no molecular hydrogen, the dust column simply traces the \hi\ column
scaled by the DGR\@. A linear fit to the relationship between the dust column
density and \nhi\ will give the DGR\@. The leftover dust column density
unassociated with \nhi\ scales with \nhtwo\ by the DGR. 

We must first divide the clouds into individual regions which have unique DGRs
and \hi\ envelopes, as discussed in \autoref{sec:cloud_boundaries}. We then
determine the \hi\ column density, \nhi, in \autoref{sec:nhi_coldens}.  Next we
quantify the calibration of the {\em Planck} $A_V$ data with the 2MASS $A_V$
data in \autoref{sec:dust_coldens} to exclude dust column density unassociated
with each cloud. Finally we complete our derivation of the DGR and its
associated uncertainty by running a bootstrap Monte Carlo simulation in
\autoref{sec:determining_dgr}.

\subsection{Cloud Boundaries}
\label{sec:cloud_boundaries}

  \begin{figure*}[!th]
    \begin{center}
    \includegraphics[width=\linewidth]{figures/multicloud_av_nhi_map.pdf}

    \caption{\label{fig:multicloud_map} \textit{Top:} Estimated \nhi\ map of
      field containing Taurus, California and Perseus. We show \hi\ integrated
      from -5 to 15 \kms, as found by \citet{lee12} for Perseus.
      \textit{Bottom:} {\em Planck} $A_V$ of the same field. The white outlines
      represent the boundaries of each region we chose. Our criteria for
      choosing cloud regions was to confine the clouds to regions where the
      background $A_V$ gradient did not vary greatly, and which included diffuse
      $A_V$ regions.
    }

    \end{center}
  \end{figure*}

  We perform our derivation of the DGR separately for each cloud. See
  \autoref{fig:multicloud_map} for a map of the three-cloud complex in $A_V$
  and an estimate of \nhi, including Perseus, Taurus, and California.  We chose
  cloud regions based on regions identified in \citep{lombardi09}.  Our
  criteria for choosing cloud regions was to confine the clouds to regions
  where the background $A_V$ gradient did not vary much, and which included
  diffuse $A_V$ regions. We also included diffuse filamentary structures which
  appeared to be associated with the cloud e.g., Taurus' North-Western arm.

  The range of the approximate \nhi\ is quite small, varying by only a factor of
  2 or so. However, each of the clouds possess a large dynamic range in $A_V$
  from 0\,mag to 10's of mag within the cores. See
  \autoref{sec:hi_htwo_properties} for a detailed description of the \hi\ and
  \htwo\ contents in each cloud.

\subsection{Determining \nhi}
\label{sec:nhi_coldens}

  Calculating \nhi\ requires a careful prescription. The steady-state models
  predicting \hi\ and \htwo\ surface densities incorporate only an atomic
  envelope around a core region. We must exclude unassociated \hi\ emission
  along the LOS.\@ 

  We select the relevant velocities of \hi\ for each cloud based on the method
  of \citet{imara11}.  We select \hi\ velocity ranges within which we assume all
  \hi\ is associated with the cloud. The broadening of the \hi\ within a cloud
  may be dominated by thermal motions, thus we fit Gaussians to the median \hi\
  spectrum of a cloud and associate the Gaussians with the CO spectrum in order
  to identify the cloud. We fit as few Gaussians as possible to accurately
  reproduce the spectra. We consider the Gaussian nearest in velocity to the
  peak median CO spectrum as the \hi\ associated with the cloud. Finally, we
  include velocities within $\pm 2\,\sigma_{\rm HI}$ of the associated \hi\
  Gaussian as our \hi\ velocity range. California has two identifiable CO
  components \citep{kong15}, thus we consider the two \hi\ Gaussian components
  nearest the CO peak as part of the California cloud. 

  The \hi\ velocity range may be uncertain from our particular Gaussian fitting,
  which Gaussians to include as part of the cloud, and how much of the
  associated cloud Gaussian we include e.g. including velocities within $\pm
  3\,\sigma_{\rm HI}$ instead of $\pm 2\,\sigma_{\rm HI}$ of the associated
  Gaussian. We adopt the minimum uncertainty on the \hi\ velocity range as the
  absolute difference between the associated Gaussian \hi\ component center and
  the median CO peak velocity. The maximum uncertainty we expect is the
  difference between the \hi\ velocity range and the range over which we see CO
  emission. We finally estimate the uncertainty in the \hi\ velocity range,
  $\delta_V$, as the average between our minimum and maximum uncertainties.

  We estimate \nhi\ by integrating \hi\ over the cloud-associated velocity
  range.  We assume optically-thin \hi.  See \autoref{fig:spectra} for the
  various spectra associated with each cloud. The \hi\ velocity ranges we find
  are all similar to the range found by \citet{lee12} of -5 to +15\,\kms, see
  also \autoref{table:cloud_properties}, and also similar to the -8 to
  +14\,\kms\ velocity range found by \citet{imara11} for Perseus. We also
  recover signatures of two CO components found by \citet{kong15} in the
  spectra. California shows a significant amount of unassociated \hi\ at lower
  velocities, which is to be expected considering California is closer to the
  Galactic plane.

  \begin{comment}

    Next, we compare the Gaussian-fit velocity range with selecting the
    relevant velocities of a cloud based on the standard deviation of the HI
    spectrum \citep{planck11_24}. The standard deviation spectrum of a region
    reveals structure more clearly than the median or mean spectra. We identify
    the \hi\ velocity range by hand, marking the edge of the range as the
    velocity where the spectrum falls to 0, or where two components in the
    spectrum meet. We use this method to show the various components within the
    three clouds in \autoref{fig:spectra}. The standard deviation spectrum does
    not reveal any structure in Perseus or Taurus not identified with the mean
    or median \hi\ spectrum. The standard deviation spectrum of California
    however shows three well-defined components. Since the median CO spectrum
    spectrum has two components, we include velocities including the two
    standard deviation components nearest the CO peak.

    The velocities chosen between the two methods are similar, see
    \autoref{table:hi_velocity_range}. However both are uncertain by at least a
    few \kms. The Gaussian-fitting is biased by the number of Gaussians we use
    to fit the spectrum, and examining the standard deviation spectrum, done by
    hand, is somewhat crude.  

  \end{comment}
 
  
  \begin{figure}[!ht]
    \begin{center}
    \includegraphics[width=\linewidth]{figures/multicloud_spectra.pdf}

    \caption{\label{fig:spectra} Global spectra of each cloud region.  The solid
      black line shows the median \hi\ spectrum, and the orange solid line
      represents the median CO line within each of the clouds. The dashed black
      lines are the Gaussian components fitted to the median \hi\ spectrum. The
      thicker black dashed line represents the component used to estimate the
      \hi\ range of the cloud. The thick reddish-orange line is the fit to the
      median \hi\ spectrum. The width of the gray regions corresponds to the
    \hi\ width derived using the method of \citet{imara11}.}

    \end{center}
  \end{figure}

  \begin{comment}
  \begin{deluxetable}{lcccc}[!ht]
    \tabletypesize{\footnotesize}
    \tablecolumns{5}
    %\tablewidth{0pt}

    \tablecaption{\hi\ Velocity Range\label{table:hi_velocity_range}}

    \tablehead{\colhead{} & 
           \colhead{\hi\ Velocity\tablenotemark{a}} &
           \colhead{$\delta_V$\tablenotemark{b}} \\
           \colhead{} &
           \colhead{Range} &
           \colhead{} \\
           \colhead{} &
           \colhead{[\kms, \kms]} &
           \colhead{\kms} \\
         }

    \startdata{}\noindent

      \input{tables/multicloud_vel_ranges.tex}
    
    \enddata{} 
      
      \tablenotetext{a}{\hi\ velocity range associated with each cloud,
      as determined by Gaussian fitting of cloud median \hi\ spectrum
    \citep{imara11}.}

      \tablenotetext{b}{Uncertainty in \hi\ velocity range, defined by
      the absolute offset between the CO median spectrum peak and
    location of cloud Gaussian.}


    %\vspace{-0.8cm} \tablecomments{}
  \end{deluxetable}
  \end{comment}

\subsection{DGR and Uncertainty}
\label{sec:determining_dgr}

  We eventually derive \nhtwo\ by scaling the dust column density by the DGR and
  subtracting \nhi. We derive the DGR by performing an
  orthogonal-distance-regression fit to the relationship between $A_V$ and \nhi.
  \autoref{fig:av_vs_nhi} shows the relationship between \nhi\ and $A_V$ as well
  as the fitted DGR. We discuss our derived DGRs with other measurements later
  in the section. The vast majority of lines-of-sight appear to demonstrate a
  linear trend between $A_V$ and \nhi.  Additionally, the relative error on the
  low-$A_V$ data is much smaller, thus the low $A_V$, which are linearly
  proportional to \nhi, dominate the fit. The median error above $A_V$ of 5\,mag
  is 1.5\,mag, while below $A_V$ of 5\,mag the median error is 0.5\,mag. The
  measurement uncertainty of \nhi\ is nearly constant for all data points at
  $\sim\,1\,\times\,10^{18}$\,cm$^{-2}$.

    \begin{figure}[!ht]
      \begin{center}
      \includegraphics[width=\linewidth]{figures/multicloud_av_vs_nhi.pdf}

      \caption{\label{fig:av_vs_nhi} $A_V$ vs. \nhi\ for each cloud. The
        contours include 99\%, 98\%, 95\%, 86\%, and 59\% of the data. The $A_V$
        and \nhi\ values shown represent the median values found in the Monte
        Carlo simulations which incorporate systematic uncertainties associated
        with estimating $A_V$ and \nhi (\autoref{sec:monte_carlo_simulations}).
        The median bootstrapped fit using all data points is shown as a
        orange-dashed line.  The median values are shown as brown squares. Our
        estimate of the DGR is insensitive to outliers, i.e. the high-$A_V$
        pixels because of the bootstrap application.  We see that Taurus shows a
        large range in \nhi\ compared to Perseus and California.  }

      \end{center}
    \end{figure}

  The DGR is much more uncertain than the variance of the fit to our single
  observations of each cloud. Systematic uncertainties of $A_V$ and \nhi\
  dominate the uncertainty of the DGR as discussed in
  Sections~\ref{sec:nhi_coldens} and~\ref{sec:dust_coldens}. Additionally we
  might expect that the regions we defined for each cloud in
  \autoref{sec:cloud_boundaries} might include lines-of-sight which may skew the
  cloud DGR from the true value, or that some lines-of-sight with molecular
  hydrogen present may skew the DGR. We thus implement a Monte Carlo simulation
  to mitigate such systematic bias and estimate the DGR uncertainty. We identify
  however, that the use of a single DGR for a cloud may only be accurate to
  within a factor of two \citet{kim96}.

  After deriving the DGR and \nhi\ for each cloud we derive \htwosd\ using
  \autoref{eq:av_dgr}. \autoref{fig:h2_maps} shows the \htwosd\ maps for each
  cloud. The \htwosd\ morphology is generally similar to $A_V$ because the \nhi\
  extends across much wider scales than the $A_V$.

  \begin{figure}[!ht]
    \includegraphics[width=\linewidth]{figures/h2_maps.pdf}

    \caption{\label{fig:h2_maps} }

  \end{figure}

  \subsubsection{Monte Carlo Simulations}\label{sec:monte_carlo_simulations}

    We follow a similar method to \citet{leroy09} where we perform Monte Carlo
    simulations to recalculate the DGR under various realizations of the data
    assuming uncertainties in both $A_V$ and \nhi. In each simulation of the
    data we perform an orthogonal-distance-regression fit to the $A_V$-\nhi\
    relationship. We use the measurement uncertainty variances of the $A_V$ and
    the \nhi\ for the weights of the fit. 
    
    We incorporate as many uncertainties in the data as possible. Below we
    outline the steps we take to include uncertainties both systematic and
    random present in the data:

    \begin{enumerate}
      
      \item Add random normal noise to each pixel based on measurement
        uncertainties in the $\tau_{353}$ map, \ebv\ calibration, $R_V$
        conversion (\autoref{sec:data_planck_models}) and \hi\ data
        (\autoref{sec:data_galfa_hi}).

      \item Derive \nhi\ by adding a normal-random offset to the lower and upper
        velocity bounds (\autoref{sec:nhi_coldens}).

      \item Rescale the {\em Planck} map. Divide the map by a uniformly-random
        number between 1 and the fitted slope between {\em Planck} and 2MASS
        $A_V$, assuming the {\em Planck} and 2MASS $A_V$ bound the true $A_V$
        (\autoref{sec:scaling_av}). Dividing the {\em Planck} map by a slope of
        one means to retain the original {\em Planck} $A_V$ values.  Dividing
        the {\em Planck} map by the slope between {\em Planck} and 2MASS means
        to bootstrap the {\em Planck} $A_V$ to the 2MASS scaling. The average
        $A_V$ will be will scale as the {\em Planck} $A_V$ divided by 1/2 of the
        fitted {\em Planck} - 2MASS $A_V$ slope in \autoref{table:planck_2mass},
        e.g., for Perseus the average $A_V$ will be the {\em Planck} $A_V$
        divided by 1.15.

      \item Include the excess dust emission offset uncertainty by adding a
        normally-random offset of scale 0.2 mag
        (\autoref{sec:background_uncertainty}).

    \end{enumerate}

    We follow the methods of \citet{chen15} and \citet{sandstrom13} who
    bootstrapped dust and gas column densities to estimate the error on the
    DGR and better isolate the linear relationship between low dust column
    density and \nhi. After simulating a new realization of the data, we
    bootstrap the data with replacement, using the same number of
    lines-of-sight. 

    We run 10,000 simulations. For each data realization we perform a orthogonal
    distance regression fit without an intercept. We report the median and 68\%
    confidence interval on the DGR in \autoref{fig:av_vs_nhi}. We confirm that
    10,000 simulations accurately measures the uncertainty in the DGR, we
    perform two more batches of 10,000 simulations. All derived DGR and
    uncertainties between the three batches are within 1\% of each other,
    assuring us 10,000 simulations accurately samples the uncertainties of the
    data.  \autoref{table:cloud_properties} summarizes the Monte Carlo
    simulations results. The distribution of the median $A_V$, median \nhi\ and
    median fitted DGR are shown in \autoref{fig:av_vs_nhi}. 

    We find a similar DGR for Perseus, 12.8$^{+6.7}_{-2.0}$\,\dgrunit, with the
    DGR found in \citet{lee12} of 11\,\dgrunit. Within our DGR uncertainty our
    results agree with \citet{lee12}.  Our derived DGR for Taurus is a little
    less than two times higher than found by \citet{paradis12}, however
    \citet{paradis12} used the entire LOS to calculate \nhi, so this discrepancy
    is not surprising. All three clouds are within the potential DGRs found
    within the Galaxy using UV absorption measurements, i.e., within a factor of
    two of $\sim\,5$\dgrunit\ \citep{kim96}.

  
  \begin{deluxetable}{ l c c c }[!ht]
    \tabletypesize{\footnotesize}
    \tablecolumns{4}
    %\tablewidth{0pt}
    \tablecaption{Cloud Properties\label{table:cloud_properties}}

    \tablehead{\colhead{Property} & 
           \colhead{California} &
           \colhead{Perseus} &
           \colhead{Taurus}
         }

    \startdata{}\noindent

      \input{tables/cloud_properties.tex} 

    \enddata{}
    %\vspace{-0.8cm}
    \tablecomments{Median Dust-to-Gas Ratio (DGR) determined from from Monte
      Carlo simulations with 68\% confidence uncertainties
      (\autoref{sec:determining_dgr}). \hi\ range determined from fitting
      Gaussians to cloud median \hi\ spectrum \citep{imara11}, with an
      uncertainty calculated as the \hi\ Gaussian peak offset from the median CO
      spectrum peak (\autoref{sec:nhi_coldens}).  $T_{\rm dust}$ average core
      region dust temperature measured from a modified SED fit from {\em Planck}
      IR maps along each LOS (\autoref{sec:rad_field}). $I_{UV}$ is the
      Lyman-Werner band (912\,-\,1108\,\angstrom) radiation field intensity
      relative to the Draine field, determined by assuming an average dust size
      and composition, and relating the dust temperature to the absorbed ambient
      radiation (\autoref{sec:rad_field}).  $G_0^\prime$ is the same as $I_{UV}$
      except relative to the Habing field (\autoref{sec:rad_field}).  Distance
      determined by stellar reddening photometry from \citet{schlafly14}.  Mass
      calculated from stellar extinction $A_k$ measurements \citep{lombardi10}.
      $N_{\rm YSO}$ is the young stellar object (YSO) count in the molecular
      cloud \citep{lada10}.  Star formation rate (SFR) calculated from the
      number of YSOs assuming a constant stellar age across the cloud
      \citep{lada10}. Metallicity determined from spectra of an A3 star by
      \citet{gonzalez09} for Perseus and T Tauri stars by \citet{dorazi11} for
      Taurus. The region size corresponds to the physical core region sizes
      shown in \autoref{fig:core_regions}.
    }

  \end{deluxetable}

    We consider the possibility of a background / foreground cloud with
    independent \hi\ and dust components. We pose the scenario where excess \hi\
    is associated with dust, both unassociated with the cloud \hi\ and dust. We
    assume all \hi\ not included in the cloud \hi\ velocity range composes the
    excess \hi\ emission. Specifically, any \hi\ with velocities below the cloud
    lower \hi\ velocity limit and any \hi\ with velocities greater than the
    higher \hi\ velocity limit is included as the excess cloud. We
    simultaneously fit for an independent DGR for each cloud and an intercept,
    i.e., a cloud DGR, a background DGR, and an intercept. We find the DGRs of
    the clouds can vary by up to 40\% when including an excess \hi\,/\,dust
    component. These variations in the DGR due to including an excess
    \hi\,/\,dust component are within our estimated uncertainty of the DGRs.
    However it would be a worthy experiment to perform a more in-depth analysis
    of a background DGR.

    % See a background fitting description and figures at:
    % http://ezbc.me/research/2015/08/19/bootstrapping/





