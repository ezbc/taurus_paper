
We find that both models consistently explain the relationship between \hi\ and
\htwo\ across 30 cores in Taurus, California and Perseus. We are unable to rule
out one model over the other given the precision of the fits to the data. Both
models explain the relationship between \hisd\ and \htwosd\ equally well. We do
however have the power to compare predictions of the CNM and average neutral gas
densities with estimated densities from observations.

We discuss the consistency of the model fits to our observations in
\autoref{sec:model_consistency}. We extend the model examination to exploring
the possibility of a multiphased neutral gas in \autoref{sec:gas_densities} and
\autoref{sec:temps} by examining the predicted gas densities and temperatures
from each model. We explain the model dependence on the radiation field, which
varies between clouds, in \autoref{sec:model_radiation}. Finally, we compare the
predicted ISM property and observed \hi\ contents spatial distribution to the
cloud environments in \autoref{sec:spatial_properties}.


If the clouds were similar, we would expect for California to contain the
majority of diffuse lines of sight since the core regions probe the farthest
sightlines from the dense core region center (because California is farther
away). We see that XX\% LOS in California are diffuse (\rhtwo$ < 1$) while XX\%
in Taurus and Perseus.

California has a lower SFR but 10 times the mass as Perseus however. 



\subsection{Model Consistency}
\label{sec:model_consistency}

  We will discuss the implications of the fitted parameters, \aG\ and \phicnm.
  \autoref{table:core_properties} shows the best-fit \aG\ value for each core,
  which are all much greater than 1. The S+14 model predicts that \hi-dust is an
  important factor in \htwo\ formation. The S+14 predicts that the \hi-to-\htwo\
  transition is sharp, consistent with the K+09 model application.  See Section
  2.2.7 of S+14 for a detailed description of \aG.  \aG\ is the ``free-space
  ratio of the \hi-dust to \htwo-line absorption rates of LW photons in the
  \htwo-dust-limited dissociation band." When \aG\,$\ll$\,1, dust associated
  with \hi\ does not affect \htwo\ formation. If \aG\,$\gg$\,1, dust associated
  with \hi\ absorbs LW photons, shielding \htwo\ from photodissociation.

  \autoref{table:core_properties} shows the fitted \aG\ and \phicnm\ values for
  each core from the S+14 and K+09 models. Nearly all cores show high \aG\
  values (\aG\,$\gg$\,1). High \aG\ values in the S+14 model imply that
  dust-shielding is more important than \htwo-self-shielding. Since the
  predicted gas densities are low, $\sim\,1 - 10$\,\voldens, the S+14 model
  predicts \hi-dust is the primary shielding component from \htwo-dissociating
  photons.

  The K+09 model \phicnm\ parameter is between 4 and 10 for most cores. This
  means that the $\chi$ parameter (the normalized radiation field similar to the
  \aG parameter, except without the \htwo\-dust contribution, see
  \autoref{eq:chi}) is around 1. For $\chi \sim 1$, the K+09 model predicts
  dust-shielding from dust associated with the CNM and \htwo\-self-shielding are
  equally important. 

  The predicted discrepancy in the dominant shielding component between the two
  models may be attributed to \hi-dust shielding associated with phases other
  than the CNM. Upon initial examination, discrepancies in the S+14 model
  between $\chi$ and \aG\ are attributed to the inclusion of \htwo\-dust
  shielding in the S+14 model. S+14, in section 4.1.4, show that $\alpha G = w
  \chi$, where $w$ is the amount of LW photons absorbed by \htwo\ dust. For high
  metallicity (large dust opacities), $w<1$, decreasing as the square root of
  the dust opacity, and then $\alpha G<\chi$.  However, we find that $\alpha
  G>\chi$.

  The only way to account for \aG$\,>\,\chi$ in the high metallicity regime is
  that the WNM / UNM are associated with significant dust components shielding
  the \htwo\ cores. This interpretation is consistent with the predicted low
  characteristic gas densities of around $\sim\,1 - 10$\,\voldens. There are a
  few cores in Taurus where \phicnm$\,>10$, leading to $\chi\,<\,1$ and \aG\ is
  a few. This would imply that the K+09 model predicts \htwo\ self-shielding is
  more important, and the S+14 model predicts \hi-dust and \htwo\ self-shielding
  are nearly equally important.

  We will compare the \hisd\ thresholds with previous extragalactic and
  galactic measurements e.g. \citet{lee12, motte14, wong09}.

\subsection{Differences in Predicted Gas Densities}
\label{sec:gas_densities}

  We calculate an ``observed" gas volume density by assuming pressure
  equilibrium between the WNM and the CNM, then calculating the gas temperature
  with the measured kinetic temperature of neutral gas from emission line \hi\
  observations in \citet{stanimirovic14}.

  K+09 does not assume any dust is associated with \htwo, thus the $\chi$
  parameter in K+09 (\autoref{eq:chi}) differs from the \aG\ term in S+14
  by an \htwo-dust-absorption factor, $w \equiv 1/[1+(2.64 \phi_g
  Z^\prime_g)^{1/2}]$.  Without this term, the predicted neutral gas densities
  will be higher for a multiphased medium. See \citet{bialy15} for a more
  extensive discussion of the consequences of including $w$ in \aG. 

  Indeed we observe the expected higher gas densities predicted by the K+09
  model. \autoref{table:core_properties} show the gas densities predicted by
  the K+09 model are an order of magnitude higher than the S+14 gas densities.
  
\subsection{Hydrogen Gas Temperatures}
\label{sec:temps}

  \subsubsection{Predicted Gas Temperatures}
  \label{sec:temp_prediction}

    Since K+09 assumes that the dust associated with the CNM is the dominant
    shielding component for dust, the predicted gas density from the K+09 model
    is of the CNM. This assumption also allows K+09 to predict the CNM
    temperature, $T_{\rm CNM}$, directly along with knowledge of metallicity and
    the CNM fraction (see Equation 19 in K+09).  

    The S+14 model does not assume that the atomic gas shielding layer is in the
    CNM phase. Instead S+14 assumes that all hydrogen gas is associated with a
    dust component, which means that the predicted number density is an average
    of all hydrogen gas in all phases. We therefore must estimate the average
    hydrogen gas temperature assuming pressure equilibrium between the gas
    phases. We adopt a stand galactic pressure of $P / k =
    3,700\,\pm\,1200$\,K\,cm$^{-3}$ \citep{jenkins11}.  See
    \autoref{table:core_properties} for a list of the derived K+09 CNM and S+14
    average gas temperatures.
  
  \subsubsection{Observed vs. Predicted Gas Temperatures}
  \label{sec:temp_obs_vs_predicted}

    We compare temperatures of the \hi\ components along LOSs from the
    \hi-absorption survey from \citet{stanimirovic14}. We include only the 15
    LOSs in our comparison within the region mapped in
    \autoref{fig:multicloud_map}. The survey decomposed emission and
    absorption \hi\ profiles to determine the spin temperatures, $T_s$ of
    individual components.

    % See
    % http://www.wolframalpha.com/input/?i=x+%2F+%281+%2B+%282.64+*+x%29%5E0.5
    % for the dependnce of n on phi_g

    % see 
    % http://www.wolframalpha.com/input/?i=1+%2F+%281+%2B+%282.64+*+x%29%5E0.5
    % for the dependnce of n on Z
    
    We compare the measured $T_s$ distribution with the K+09 CNM temperature
    and the average LOS spin temperature with the S+14 predicted gas
    temperature distribution. The high sensitivity \citet{stanimirovic14} is
    (RMS optical depth noise level of 10$^{-3}$\,\kms) allowed them to acquire
    spin temperatures of individual components up to 1,000\,K, sampling much of
    the CNM components. Likely the most direct comparison of the predicted CNM
    temperature predicted by the K+09 model may be $T_s$.  However, the S+14
    model predicts average temperature of all hydrogen gas along the line of
    sight. The spin temperatures of individual components in
    \citet{stanimirovic14} are biased towards CNM temperatures due to the
    sensitivity limits. Perhaps the best option is to use the
    optical-depth-weighted average spin temperature, $<T_s>$, along each line
    of sight to compare to the average temperature predicted from S+14, given by
    
    \begin{equation}
      <T_s> = \frac{\int \frac{T_B}{1 - e^{-\tau}} \tau\,{\rm d}v }
              {\int \tau\,{\rm d}v}
    \end{equation}

    \noindent where $T_B$, the measured brightness temperature, and $\tau$, the
    measured optical depth, are integrated over velocity, $v$. Since the
    optical depth of a gas component is proportional to the column density, the
    optical depth should also be proportional to the gas density along the
    LOS.  The optical-depth-weighted average temperature may be the closest
    measure of the average temperature of the gas along the line of sight.

    See \autoref{fig:density_cdf} for a comparison of the cumulative
    distributions functions (CDFs) of the observed and predicted K+09 CNM
    temperatures, and the observed and predicted S+14 average hydrogen gas
    temperatures. The distributions of the predicted CNM temperatures from K+09
    agree well with the observed spin temperatures; the distributions have
    similar medians of around 60\,K. However the optical-depth-weighted average
    hydrogen temperature and the predicted hydrogen gas temperature from the
    S+14 model do not agree as well; the medians of the distributions differ by
    about a factor of 2. 
    
    There are many caveats involved in estimating these temperatures. A robust
    comparison of the predicted vs. observed temperatures is beyond the scope
    of this paper. We plan to perform a more extensive comparison with
    additional absorption {\em Very Large Array} observations on-hand.

    \begin{figure}[!ht]
      \includegraphics[width=\linewidth]{figures/density_cdf.pdf}

      \caption{\label{fig:density_cdf} Cumulative distributions of hydrogen
        volume densities predicted and observed within the
        Taurus-California-Perseus region. $n_s$ is the volume density of
        hydrogen calculated from the spin temperatures for individual components
        along the line of sight in \hi\ absorption-line measurements, assuming
        pressure equilibrium in the hydrogen gas. The individual $n_s$ values
        correspond to the estimated densities of the CNM components, thus should
        be comparable with the predicted K+09 predicted \ncnm. Next is the
        observed average volume number density of hydrogen, $<n_s>$, calculated
        in a similar way as $n_s$ except from the optical-depth-weighted average
        spin temperature, $<T_s>$, along the line of sight. $<n_s>$ should be
        comparable with S+14 given that S+14 predicts a characteristic hydrogen
        gas volume density along the line-of-sight, $n_H$. The shaded regions
        correspond to point-wise 68\% confidence intervals of each parameter
        derived from a suite of Monte Carlo simulations, varying pressures in
        hydrogen gas expected in the Milky Way.}

    \end{figure}

    \begin{figure}[!ht]
      \includegraphics[width=\linewidth]{figures/modelparams_cdf.pdf}

      \caption{\label{fig:density_cdf} Cumulative distributions of fit \phicnm\
        and \aG\ values for each cloud. 
      }

    \end{figure}
    
    
    \begin{comment}
    \citet{stanimirovic14} found that only 10-50\% of the neutral gas in Perseus
    was in the cold phase. They suggested that even deep within molecular clouds
    do not have CNM fractions > 50\%.
    \end{comment}


\subsection{Model Dependence on Radiation Field}
\label{sec:model_radiation}

  Here we will discuss how the free-space radiation field in each cloud affects
  the \hi\-to-\htwo\ transition, and the associated uncertainties.

  The radiation field primarily determines the atomic gas column in the K+09
  model, which they characterize as dimensionless parameter of the rate of LW
  photon absorption by dust grains to \htwo, $\chi$. The optical depth of the
  CNM \hi\ shielding layer is approximately proportional to the radiation field
  strength (see Equation 9 in K+09), thus the \hisd\ threshold is proportional
  to the radiation field strength as well. 

  However the S+14 predicts the threshold \hisd\ weakly depends on the
  cloud-averaged incident interstellar radiation field. In the S+14 model, the
  transition \hisd\,$\propto\,\log[I_{\rm UV}]$ (see
  Equations~\ref{eq:s+14_hisd} and ~\ref{eq:alphaG}).

\subsection{Spatial Distribution of Predicted and Observed ISM Properties}
\label{sec:spatial_properties}

  In this section we will discuss the fitted parameters of the models, \phicnm\
  and \aG, and the predicted ISM properties from the S+14 model, the \hisd\
  threshold, $n_H$ and $T_H$. We can relate properties of each cloud to the
  predicted ISM properties, such as the SFR and molecular gas contents.

  \begin{figure}[!ht]
    \begin{center}
    \includegraphics[width=\linewidth]{figures/multicloud_av_ISMparams_map.pdf}

    \caption{\label{fig:ism_params_map} Spatial distributions of the following
      values from top to bottom: $\phi_{\rm CNM}$ (K+09 model) representing the
      ratio between the observed CNM volume density and the CNM volume density
      necessary to remain in pressure equilibrium with the WNM; $\alpha G$
      (S+14 model) represents the dust-absorption efficiency of
      \htwo\-dissociating photons; $\Sigma_{\rm HI,trans}$ is the \hisd\ value
      in the S+14 model where $R_{H2}$ is 1; $n_H$ is the neutral hydrogen
      volume density predicted from \citet{sternberg14} determined from
      \autoref{eq:alphaG}. $T_{\rm H}$ is the neutral hydrogen core
      envelope temperature predicted from \citet{sternberg14} assuming a
      typical neutral gas pressure of 3,000 K\,cm$^{-3}$. The contours
      represent $A_V$ at 2, 4, 8, and 16\,mag. We can immediately see that
      Taurus shows much more variation in both \phicnm\ and \aG\ than in
      Perseus and California. Since the S+14 model predicts a sharp
      \hi-to-\htwo\ transition, there is significant variation in $\Sigma_{\rm
      HI,trans}$ across Taurus.  
    }

    \end{center}
  \end{figure}

  %\citet{tatematsu04} used N$_2$H+ observations of Taurus to explore the
  %pressures involved in star formation. See Table 3 for a list of derived
  %critical pressures, which are on order of a few times
  %$10^{5}$\,K\,cm$^{-3}$.


\begin{comment}
\subsection{Relating the H\,$\textsc{i}$-to-H$_2$ Transition to $A_V$}
\label{sec:hi_av_transition}

  \citet{lombardi15}  compared several molecular cloud PDFs using
  Planck/Herschel data, and found large variations in the $A_k$ PDFs given
  different boundaries and background subtractions for $A_k < 0.1$\,mag. To
  be safe, we compare our PDFs for $A_V > 1$ mag. 

  The peak of California's PDF is about $A_V = 2$ mag. Lombardi et al. (2015)
  shows a peak of around $A_K = 0.2$\,mag. This is about 1.8\,mag. This
  difference is within the error in the intercept derived between Planck and
  2MASS $A_V$.

  The PDF of Perseus seems to show similarity to that of Burkhart et al.
  (2015) (B+15). Our PDF peak is slightly shifted to $A_V$ of 1\,mag, compared
  to the $A_V$ peak of 0.8\,mag in B+15. This is expected because of the
  positive offset we calculated for Perseus $A_V$. The \hi\ peak is slightly
  higher than in B+15 by about 0.1\,mag. This could be due to our higher DGR,
  different region selection, and slightly different \hi\ range.

  A better comparison with B+15 would be to measure the widths of Gaussians
  fitted to the rising side of the HI and $A_V$ PDFs.

  The dust and \hi\ in Taurus is less centrally concentrated than in Perseus
  and California. Lombardi et al. (2015) shows a peak of about $A_V$ = 1\,mag,
  similar to what we have found.

  %-----------------------------------------------------------------------
  \begin{figure}[!ht]
    \includegraphics[width=\linewidth]{figures/multicloud_pdfs.pdf}

    \caption{\label{fig:pdfs} Normalized Probability Density Functions (PDFs)
      of the atomic gas, molecular gas, and optical dust extinction ($A_V$) of
      California, Perseus, and Taurus. We used Planck IR observations to
      measure $A_V$, and GALFA-\hi\ observations to measure \hisd. The \htwosd\
      and \hisd\ were scaled by the dust-to-gas ratio of each cloud to be
      compared with $A_V$ (see \autoref{table:cloud_properties}). The PDFs
      are likely uncertain for $A_V < 1$\,mag due to background subtraction and
      region selection uncertainties \citep{lombardi15}. The gray shaded region
      shows the range of the \hi-to-\htwo\ transitions of the cores in each
      cloud.  The \hi-to-\htwo\ transition in Perseus corresponds to the $A_V$
      PDF peak, as found in \citet{burkhart15}. However the transitions for
      Taurus and California do not align with the $A_V$ PDFs.
    }
  
  \end{figure}
  %-----------------------------------------------------------------------

\end{comment}




  

