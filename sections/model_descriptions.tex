
%In this section we outline the two steady-state models predicting \hisd\ and
%\htwosd\ for individual core regions. Both models, \citet[][K+09]{krumholz09}
%and \citet[][S+14]{sternberg14}, predict an \htwo\ / \hi\ ratio, \rhtwo, as a
%function of total gas surface density, \hsd:

%  \begin{equation}
%    R_{\rm H_2} = 
%                  \frac{\Sigma_{\rm H}}{\Sigma_{\rm H\,\textsc{i}}} - 1 = 
%                  \frac{\Sigma_{\rm H_2}}{\Sigma_{\rm H\,\textsc{i}}}
%  \end{equation}

%\noindent where \htwosd\ and \hisd\ are observables. Another interpretation is
%both models predict \hisd\ given \hsd. The prediction of \rhtwo\ in both models
%is for a single core with an atomic envelope and a molecular core. Our
%observations of \htwosd\ and \hisd\ will enable us to compare \rhtwo\ as a
%function of \hsd\ for many individual cores possessing varying \rhtwo\ with the
%model predictions. 

In this section, we outline the two models of \htwo\ formation,
\citet[][K+09]{krumholz09} and \citet[][S+14]{sternberg14}.  Based on the
assumption of \htwo\ formation in steady state and chemical equilibrium, both
models predict how the molecular-to-atomic mass ratio (\rhtwo) changes as a
function of total gas surface density (\hsd): 

\begin{equation}
R_{\rm H_{2}} = \frac{\Sigma_{\rm H_2}}{\Sigma_{\rm H\,\textsc{i}}} 
              = \frac{\Sigma_{\rm H}}{\Sigma_{\rm H\,\textsc{i}}} - 1. 
\end{equation}

\noindent The predictions are summarized as follows and we refer to K+09 and
S+14 for details. 

\subsection{Krumholz et al. (2009) Model}
\label{sec:krumholz_model} 

  K+09 built on earlier work by \citet[][hereafter K+08]{krumholz08}, where
  H$_{2}$ formation and photodissociation were modeled for a spherical cloud
  embedded in a uniform and isotropic ISRF.  Assuming that the \hi-to-\htwo\
  transition is infinitely sharp, the location of the transition was solely
  determined by two dimensionless numbers:

  \begin{equation} \label{eq:k09_chi}
  \chi = \frac{f_{\rm diss} \sigma_{\rm g} c E_{\rm 0}^{\ast}}{n_{\rm H\,\textsc{i}} \mathcal{R}}
  \end{equation}

  \begin{equation} \label{eq:k09_tauR}
  \tau_{\rm R} = n_{\rm H\,\textsc{i}} \sigma_{\rm g} R,  
  \end{equation}

  \noindent where $f_{\rm diss} \sim 0.1$ is the fraction of LW photon
  absorption that produces H$_{2}$ dissociation rather than radiative decay to a
  bound state, $\sigma_{\rm g}$ is the dust grain absorption cross section per
  hydrogen nucleus for the LW band (cm$^{2}$), $E_{\rm 0}^{\ast}$ is the number
  density of LW photons (cm$^{-3}$), $n_{\rm H\,\textsc{i}}$ is the number
  density of hydrogen nuclei in the atomic shielding layer (cm$^{-3}$),
  $\mathcal{R}$ is the H$_{2}$ formation rate coefficient (cm$^{3}$ s$^{-1}$),
  and $R$ is the cloud radius (cm). The surface density of the atomic shielding
  layer was then predicted as 

  \begin{equation} \label{eq:k09_sigmaHI} 
  \Sigma_{\rm H\,\textsc{i},s} = \tau_{\rm H\,\textsc{i}}\left(\frac{\mu_{\rm H}}{\sigma_{\rm g}}\right), 
  \end{equation}

  \noindent where 
  \begin{equation} \label{eq:k09_tauHI} 
  \tau_{\rm H\,\textsc{i}} \approx \frac{\tau_{\rm R} \psi}{4 \tau_{\rm R} - 0.7 \psi}
  \end{equation}

  \noindent and 
  \begin{equation} \label{eq:k09_psi} 
  \psi = \chi \frac{2.5 + \chi}{2.5 + \chi e}. 
  \end{equation}

  \noindent Here $\tau_{\rm H\,\textsc{i}}$ is the dust optical depth through
  the atomic shielding layer and $\mu_{\rm H}$ is the mass per hydrogen
  nucleus. 

  By definition, $\chi$ is the ratio of the rate at which LW photons are
  absorbed by dust grains (``\hi-dust'' absorption) to the rate at which they
  are absorbed by H$_{2}$ molecules (``H$_{2}$-line'' absorption).
  Alternatively, $\chi$ can be considered as a dimensionless measure of the
  strength of the ISRF.  In strong radiation fields, many H$_{2}$ molecules are
  photodissociated and LW photons are hence mostly absorbed by dust grains
  (large $\chi$).  On the contrary, in weak radiation fields, more H$_{2}$
  molecules survive and absorb most of LW photons thanks to their large resonant
  cross section (small $\chi$).  Finally, $\tau_{\rm R}$ is simply a measure of
  the cloud size, as it is the dust optical depth that the cloud would have if
  the density throughout were the same as its density in the atomic shielding
  layer.

  For atomic-molecular complexes where $n_{\rm H\,\textsc{i}}$ and $E_{\rm
  0}^{\ast}$ are difficult to measure (e.g., extragalactic star-forming
  regions), K+09 simplified Equations \ref{eq:k09_chi} and \ref{eq:k09_tauR} by
  making two assumptions: (1) the cold and warm \hi\ (cold neutral medium (CNM)
  and warm neutral medium (WNM) respectively) are approximately in pressure
  equilibrium and (2) the CNM dominates shielding from LW photons due to its
  higher density.  In the conditions of pressure balance between the cold and
  warm \hi, the CNM density is correlated with the radiation field as follows
  (e.g., \citeauthor{wolfire03} 2003): 

  \begin{equation} \label{eq:k09_nCNM} 
  n_{\rm CNM} = \phi_{\rm CNM}n_{\rm min} 
              = \phi_{\rm CNM}\frac{31 G_{0}'}{1 + 3.1Z'^{0.365}}, 
  \end{equation} 

  \noindent where $G_{0}'$ is the ambient UV radiation field (in units of the
  Draine field) and $Z'$ is the metallicity. The primes indicate that the
  quantities are normalized to their values in the solar neighborhood. Finally,
  $n_{\rm min}$ is the minimum CNM density at which the CNM can co-exist with
  the WNM in pressure balance and $\phi_{\rm CNM}$ is typically $\sim$1--10. 

  As $n_{\rm H\,\textsc{i}}$ $\approx$ $n_{\rm CNM}$ $\propto$ $G_{0}'$
  $\propto$ $E_{\rm 0}^{\ast}$, $n_{\rm H\,\textsc{i}}$ and $E_{\rm 0}^{\ast}$
  drop out of \autoref{eq:k09_chi} and $\chi$ becomes a function of $Z'$ and
  $\phi_{\rm CNM}$ only: 

  \begin{equation} \label{eq:k09_new_chi} 
    \chi = 2.3 \left(\frac{\sigma_{\rm g,-21}}
    {\mathcal{R}_{-16.5}}\right) \frac{1 + 3.1 Z'^{0.365}}{\phi_{\rm CNM}},  
  \end{equation}

  \noindent where $\sigma_{\rm g,-21}$ = $\sigma_{\rm g}$/(10$^{-21}$ cm$^{-2}$)
  and $\mathcal{R}_{-16.5}$ = $\mathcal{R}$/(10$^{-16.5}$ cm$^{3}$ s$^{-1}$).
  $\sigma_{\rm g}$ and $\mathcal{R}$ in \autoref{eq:k09_chi} were additionally
  canceled out since both quantities are measures of the surface area of dust
  grains and their ratio is hence expected to vary little with galactic
  environments. Based on this simplified expression for the dimensionless
  radiation field, K+09 provided an analytic prediction for \rhtwo: 

  \begin{equation} \label{eq:k09_RH2} 
  R_{\rm H_{2}} = \frac{4 \tau_{\rm c}}{3 \psi} \left[1 + \frac{0.8 \psi \phi_{\rm mol}}{4 \tau_{\rm c} + 
                 3(\phi_{\rm mol} - 1) \psi}\right] - 1,
  \end{equation}
  \noindent where 
  \begin{equation}
  \tau_{\rm c} = \frac{3}{4} \left(\frac{\Sigma_{\rm comp} \sigma_{\rm g}}{\mu_{\rm H}}\right). 
  \end{equation}

  \noindent Here $\tau_{\rm c}$ is the dust optical depth the cloud would have
  if \hi\ and \htwo\ contents were uniformly mixed and $\Sigma_{\rm comp}$ is
  the total gas surface density ($\Sigma_{\rm H\,\textsc{i}} + \Sigma_{\rm
  H_{2}}$). $\phi_{\rm mol}$ is the ratio of the \htwo\ density to the CNM
  density and K+09 adopted $\phi_{\rm mol}$ = 10 as their fiducial value. 

  The primary prediction by K+09 is that $\chi$ is $\sim$1 across galactic
  environments, implying that both dust shielding and H$_{2}$ self-shielding are
  important for H$_{2}$ formation. As a consequence of $\chi$ $\sim$ 1, the
  atomic envelope around a molecular cloud has a characteristic shielding
  surface density of $\Sigma_{\rm H\,\textsc{i},s}$.  This minimum \hi\ surface
  density required for \htwo\ formation shows a weak dependence on metallicity
  only, as $\tau_{\rm H\,\textsc{i}}$ slightly increases with $Z'$ via
  $\tau_{\rm R}$ and $\psi$ and $\sigma_{\rm g}$ increases with $Z'$ via
  $\sigma_{\rm g}$ $\propto$ $Z'$. 
  %$\sigma_{\rm g}$ = $\sigma_{g}'Z'$.  
  %($\sigma_{\rm g}'$ = 10$^{-21}$ cm$^{-2}$ in the solar neighborhood). 
  For solar metallicity, $\Sigma_{\rm H\,\textsc{i},s}$ $\sim$ 10 M$_{\odot}$
  pc$^{-2}$. In the K+09 model, once the minimum \hi\ surface density is
  achieved, all additional hydrogen columns are fully converted into H$_{2}$,
  resulting in a uniform $\Sigma_{\rm H\,\textsc{i}}$ $\approx$ $\Sigma_{\rm
  H\,\textsc{i},s}$ distribution and a linear increase of $\Sigma_{\rm H_{2}}$
  with $\Sigma_{\rm H\,\textsc{i}} + \Sigma_{\rm H_{2}}$. 

\subsection{Sternberg et al. (2014) Model}
\label{sec:sternberg_model}

  S+14 presented an analytic theory of the \hi-to-\htwo\ transition for
  one-dimensional plane-parallel slabs illuminated by photodissociating UV
  radiation fields.  In the S+14 model, the fundamental parameter that controls
  the \hi-to-\htwo\ transition was

  \begin{multline} \label{eq:s14_alphaG} 
  \alpha G = \frac{D_{0} G}{\mathcal{R} n} = 
    f_{\rm diss}\frac{\sigma_{\rm g} w F_{0}}{\mathcal{R} n} 
          =\\
          1.54 \frac{G_{0}'}{(n/100~{\rm cm}^{-3})}
          \frac{\phi_{\rm g}}{1 + (2.64 \phi_{\rm g} Z')^{1/2}}, 
  \end{multline} 

  \noindent where 
  \begin{equation} \label{eq:s14_sigmag}
  \sigma_{\rm g} = 1.9 \times 10^{-21} \phi_{\rm g} Z'~{\rm cm^{2}}.
  \end{equation}

  \noindent The total \hi\ column of the slab irradiated by an isotropic ISRF
  was then given as 

  \begin{equation} \label{eq:s14_SigmaHI} 
  \Sigma_{\rm H\,\textsc{i}}~\rm{S+14} = 
  6.71 \left(\frac{1.9}{\sigma_{\rm g,-21}} \right ) {\rm ln} 
  \left[\frac{\alpha G}{3.2} + 1 \right]~{\rm M_{\odot}~pc^{-2}}.  
  \end{equation}

%  \noindent simply resulting in 

%  \begin{equation} \label{eq:s14_RH2} 
%  R_{\rm H_{2}} = \frac{\Sigma_{\rm H}}{\Sigma_{\rm H\,\textsc{i}}} - 1. 
%  \end{equation} 

  \noindent Here $D_{0}$ is the free-space H$_{2}$ photodissociation rate
  (s$^{-1}$), $G$ is the mean H$_{2}$ self-shielding factor, $n$ is the total
  hydrogen density (cm$^{-3}$), $w$ is the normalized H$_{2}$-dust-limited
  dissociation bandwidth, $F_{0}$ is the total LW photon flux (cm$^{-2}$
  s$^{-1}$), and $\phi_{\rm g}$ is a factor of order unity depending on dust
  grain properties (e.g., grain composition and scattering/absorption
  properties, etc.).  We refer S+14 for details on all these quantities. 
   
  In the S+14 model, the dimensionless parameter $\alpha G$ is the free-space
  ratio of the \hi-dust to \htwo-line absorption rates of LW photons in the
  presence of ``\htwo-dust'' attenuation.  The physical meaning of $\alpha G$ is
  essentially the same as that of $\chi$ in K+09, except that the absorption of
  LW photons by dust grains associated with H$_{2}$ molecules (``\htwo-dust''
  absorption) was considered in S+14.  In other words, $w \chi$ = $\alpha G$
  where 

  \begin{equation}\label{eq:s14_w}
    w = \frac{1}{1 + (2.64 \phi_g Z^\prime_g)^{1/2}}.
  \end{equation}

  \noindent with $w \lesssim 1$. 
% (see \autoref{sec:k09_s14_compare} for details).
  
  As for the nature of the \hi-to-\htwo\ transition, an important distinction
  occurs between the two extreme $\alpha G$ limits: ``weak-field'' limit with
  small $\alpha G \ll  1$ and ``strong-field'' limit with large $\alpha G \gg
  1$.  In the weak-field limit, \hi-dust absorption is negligible (\htwo-line
  and \htwo-dust are the primary sources of opacity) and the \hi-to-\htwo\
  transition is gradual with most of the \hi\ column built up in the
  molecular-dominated region.  On the contrary, in the strong-field limit, the
  \hi\ column becomes so large that \hi-dust dominates the attenuation of LW
  photons.  Due to the exponential nature of \hi-dust absorption, the
  \hi-to-\htwo\ transition is sharp and most of the \hi\ column is produced near
  the transition point.  

\subsection{Comparison between the Two Models} 
\label{sec:k09_s14_compare} 

  As outlined in Sections \ref{sec:krumholz_model} and
  \ref{sec:sternberg_model}, K+09 and S+14 have slightly different approaches to
  model the \hi-to-\htwo\ transition.  We summarize main differences between the
  two models as follows (see Sections 2.2.9 and 4 in S+14 for more detailed
  discussions). 

  \textit{(1) Geometry.} One of the primary differences is the assumed geometry
  for interstellar clouds: sphere (K+09) versus plane-parallel slab (S+14).  The
  calculation of radiative transfer equations is more complicated for spheres,
  as it requires a treatment of the angular dependence of the radiation field
  inside a cloud.  For example, oblique rays can go through a spherical cloud
  even if an optically thick \htwo\ core exists.  On the contrary, all rays are
  absorbed in an optically thick slab.  Based on careful comparisons, however,
  S+14 found that the predicted \hi\ columns are very similar for spheres and
  planar slabs embedded in isotropic radiation fields: the difference was less
  than 20\% for 0.01 $\lesssim$ $\alpha G$ $\lesssim$ 100. 

%  \textit{(2) \htwo-dust.} As briefly explained in
%  \autoref{sec:sternberg_model}, K+09 did not consider the attenuation of LW
%  photons by \htwo-dust.  This results in the relation of $w \chi$ = $\alpha G$,
%  where the factor $w$ in S+14 accounts for the absorption of LW photons by
%  \htwo-dust.  In the low-$Z'$ and small-$\sigma_{\rm g}$ case, $w$ = 1 as
%  \htwo-dust is negligible.  For high $Z'$ and large $\sigma_{\rm g}$ values, on
%  the other hand, $w$ < 1 and hence $\alpha G$ < $\chi$ (e.g., $w$ = 0.4 for
%  $\phi_{\rm g}$ = 1, $Z'$ = 1, and $\sigma_{\rm g}$ = 1.9 $\times$ 10$^{-21}$
%  cm$^{2}$). 

  \textit{(2) Two-phase Equilibrium.} Finally, K+09 assumed that the cold and
  warm atomic phases co-exist in pressure balance and dust grains associated
  with the cold component dominate shielding against dissociating LW photons.
  The direct consequence of these assumptions is the invarince of $\chi$ $\sim$
  1 across galactic environments, as $G_{0}'$/$n_{\rm CNM}$ is restricted to a
  narrow range in the condition of two-phase equilibrium (see
  \autoref{eq:k09_nCNM}).  On the other hand, S+14 did not make an assumption
  about \hi\ phases, hence constraining the effective \hi\ density. 

