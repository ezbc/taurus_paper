
We compare our observations of the California, Perseus, and Taurus molecular
clouds with the K+09 and S+14 models by fitting \rhtwo as a function of \hsd.
For this purpose, we employ our 20,000 Monte Carlo simulations in
\autoref{sec:methodology},  where $A_V$ and \nhi\ are perturbed based on their
uncertainties and $\sigma_{\rm g}$ is re-derived accordingly.  In the following
sections, we describe model analyses in details.   

\subsection{Input Model Parameters: $Z'$ and $\sigma_{\rm g}$}
\label{sec:input_model_parameters}

As Equations \ref{eq:k09_new_chi}, \ref{eq:k09_RH2}, \ref{eq:s14_alphaG}, and
\ref{eq:s14_SigmaHI} suggest, $Z'$ and $\sigma_{\rm g}$ are the two important
parameters for determining \rhtwo\ in K+09 and S+14.  In our model analysis, we
use $Z'$ = 1 (solar metallicity) for all three modelcular clouds.  This use of
$Z'$ = 1 is based on the measurements of stellar metallicity
(\autoref{table:cloud_summary}): optical spectroscopic observations of T Tauri
stars and an A3 V-type star in Taurus and Perseus found $Z$ $\sim$ 1 Z$_{\odot}$
(\citeauthor{gonzalez09} 2009; \citeauthor{dorazi11} 2011).  The same solar
metallicity is assumed for California, considering that the cloud is located
close to Perseus and Taurus (within $\sim$100--200 pc).  Our assumption of a
single metallicity is reasonable, as one DGR is found to fit most of the diffuse
regions in each cloud (\autoref{sec:determining_dgr}). 

As for $\sigma_{\rm g}$, we scale the typical value of 1.9 $\times$ 10$^{-21}$
cm$^{2}$ (e.g., \citeauthor{draine96} 1996) based on our DGR estimate in each
Monte Carlo simulation.  For example, if the simulated DGR is 11.6 $\times$
10$^{-22}$ mag cm$^{2}$, which is $\sim$2.2 times higher than the canonical
Galactic value of 5.3 $\times$ 10$^{-22}$ mag cm$^{2}$, a scaled $\sigma_{\rm
g,-21}$ = 4.2 is used for both models.  In the S+14 model, a higher $\sigma_{\rm
g}$ results from either some variations in dust grain properties (encapsulated
by $\phi_{\rm g}$) or higher dust abundances (encapsulated by $Z'$).  With the
fixed $Z'$ = 1 in our model analysis, $\phi_{\rm g}$ is therefore scaled
according to each simulated DGR value.  Throughout our 20,000 Monte Carlo
simulations, the median and 68\%-confidence uncertainty of $\sigma_{\rm g}$ are
found to be $\sigma_{\rm g,-21}$ = 2.7$^{+1.7}_{-0.9}$, 4.1$^{+2.4}_{-1.1}$, and
3.8$^{+3.7}_{-1.2}$ for California, Perseus, and Taurus respectively.  We note
that the variations in $\sigma_{\rm g}$ are in fact not unexpected, as dust
evolution (e.g., coagulation of dust grains and/or accretion of gas-phase metals
onto dust grains) and subsequent changes in dust properties have been noted in
and around molecular clouds both observationally and theoretically (e.g.,
\citealt{roman-duval14}; \citealt{kohler15}). 

% dust properties:
% DGRs = 7.6, 11.6, and 10.7 10^-22 cm^2 mag for CPT
% galactic DGR = 5.3 10^-22 cm^2 mag
% scalar = 7.6/5.3, 11.6/5.3, 10.7/5.3 for CPT
% phi_g = 1.43, 2.19, 2.018
% sigma_g = 1.9 * 1.43, 1.9 * 2.19, 1.9 * 2.02
% sigma_g = sigma_d = 2.72, 4.16, 3.84

\subsection{Best-fit Models} 
\label{sec:best_fit_models}

  To find best-fit K+09 and S+14 models, we perform unweighted orthogonal
  distance regression fitting to each simulated \rhtwo\ vs. \hsd\ curve,
  constraining $\phi_{\rm CNM}$ for K+09 and \aG\ for S+14.  As for K+09,
  $\phi_{\rm mol}$ = 10 is adopted for all three molecular clouds, following
  what \citet{lee12} did for Perseus\footnote{Note that our fitting results do
    not heavily depend on the adopted $\phi_{\rm mol}$ value, as \rhtwo\ is not
    sensitive to $\phi_{\rm mol}$ (e.g., a factor of $\sim$1.1 change in \rhtwo
  when \hsd\ = 100 M$_{\odot}$ pc$^{-2}$ and $\phi_{\rm CNM}$ = 1--10).} Among
  the 20,000 simulated $\phi_{\rm CNM}$ and \aG\ values, the 16th, 50th
  (median), and 84th percentiles of the distribution are then used as the lower
  uncertainty, best-fit estimate, and upper uncertainty of the model parameter
  respectively.  In addition, the best-fit estimate and the lower/upper error
  margins are fed into Equations \ref{eq:k09_nCNM}, \ref{eq:k09_new_chi},
  \ref{eq:s14_alphaG}, and \ref{eq:s14_SigmaHI} to calculate $n_{\rm CNM}$,
  $\chi$, $n$, $\Sigma_{\rm H\,\textsc{i}}$ S+14 and their uncertainties.  To be
  compared with $\Sigma_{\rm H\,\textsc{i}}$ S+14, $\Sigma_{\rm H\,\textsc{i}}$
  K+09 is derived as the $\Sigma_{\rm H\,\textsc{i}}$ value where the K+09 model
  predicts \rhtwo = 1.  We present the observed \hisd\ vs. \hsd\ and \rhtwo vs.
  \hsd\ curves along with the best-fit K+09 and S+14 models in Figures
  \ref{fig:california_hi_vs_h}, \ref{fig:perseus_hi_vs_h},
  \ref{fig:taurus_hi_vs_h}, \ref{fig:california_rh2_vs_h},
  \ref{fig:perseus_rh2_vs_h}, and \ref{fig:taurus_rh2_vs_h} and summarize the
  model parameters in \autoref{table:core_properties}.

\subsection{Model Fitting Results}
\label{sec:model_fitting_results} 

  \subsubsection{\hi Saturation}
  \label{sec:model_results1}

    We find that both the K+09 and S+14 models reproduce well our observations
    of California, Perseus, and Taurus, as clearly shown in Figures
    \ref{fig:california_hi_vs_h}, \ref{fig:perseus_hi_vs_h},
    \ref{fig:taurus_hi_vs_h}, \ref{fig:california_rh2_vs_h},
    \ref{fig:perseus_rh2_vs_h}, and \ref{fig:taurus_rh2_vs_h}\footnote{The less
      apparent agreement with the two models in Figures
      \ref{fig:california_hi_vs_h}, \ref{fig:perseus_hi_vs_h}, and
      \ref{fig:taurus_hi_vs_h} (e.g., G159.19-20.11 in Perseus and G174.70-15.47
      in Taurus) arises from the fact that model fitting is biased toward the
      data points with high column densities where \hi saturation is most
      clear.  The slight offset from the model predictions at low column
      densities is more pronounced in our \hisd\ vs. \hsd\ curves as the plots
      are on linear scales.  Note, however, that the offset is small (within the
    systematic uncertainty in \hisd) and \hisd\ is extremely uniform across the
  individual core regions as K+09 and S+14 predict.}.  This excellent agreement
  with the models is entirely driven by the saturation of \hisd, resulting in
  the characteristic shape of the observed \rhtwo vs. \hsd\ curves in log-linear
  space: \rhtwo sharply rising at the \hisd\ threshold and gradually increasing
  once \hisd\ $\sim$ \htwosd.  Specifically, \hisd saturates to
  $\sim$9--11\,\msunpc, $\sim$6--9\,\msunpc, and $\sim$4--9\,\msunpc for
  California, Perseus, and Taurus respectively.  Among the three clouds, Taurus
  shows the largest variation in \hisd, by up to a factor of $\sim$2. 

    We note that the good agreement with K+09 and S+14 most likely holds even
    when the presence of cold \hi is considered.  In our study, \hisd\ is
    estimated under the assumption of optically thin \hi emission and a large
    amount of cold \hi can alternatively result in the saturation of \hisd.
    This issue of the impact of high optical depth on the \hi column density
    distribution was recently explored by \citet{lee15} for Perseus.  In
    essence, \citet{lee15} used Arecibo \hi absorption measurements made in the
    direction of 26 radio continuum sources located behind Perseus
    (\citeauthor{stanimirovic14} 2014) to derive the empirical correction factor
    ($f$) for high optical depth, $f$ = log$_{10}$($N_{\rm
    exp}$/10$^{20}$)($0.32 \pm 0.06)$ + ($0.81 \pm 0.05$), where $N_{\rm exp}$
    is the optically thin \hi column density.  The derived correction factor was
    applied to the \hi column density image of Perseus on a pixel-by-pixel
    basis, resulting in only a $\sim$10\% increase in the total \hi mass.  If we
    extrapolate the empirical relation by \citet{lee15} to California and
    Taurus, we find that the correction factor for the optically thick \hi is
    only up to $\sim$1.2, essentially not changing our \hisd\ vs. \hsd\ and
    \rhtwo vs.  \hsd\ curves.  An independent analysis of \hi absorption
    observations to revisit the saturation of \hisd\ in California and Taurus is
    currently being undertaken and will be presented in a forthcoming paper (Lee
    et al., in prep).  Additional evidence supporting the insignificant amount
    of cold \hi can be inferred from the studies of \hi narrow self-absorption
    (HINSA) in Perseus and Taurus (\citep{li03,krco10}): while frequently
    observed, HINSA features due to the presence of cold \hi comprise only up to
    $\sim$2\% of the total column density. 

  \subsubsection{Extended \htwo Distributions} 
  \label{sec:model_results2} 

    Another interesting result from Figures \ref{fig:california_hi_vs_h},
    \ref{fig:perseus_hi_vs_h}, and \ref{fig:taurus_hi_vs_h} is that we do not
    generally probe purely atomic regions in California, Perseus, and Taurus,
    which can be seen as a lack of many data points on the linear portion of the
    \hisd\ vs.  \hsd\ curves.  Except for a couple cores in Taurus
    (G169.32-17.17 and G168.10-16.38), the \hsd\ values are all above the
    systematic uncertainty of 5\,\msunpc, suggesting that the absence of purely
    atomic regions is due to highly extended \htwo distributions rather than low
    sensitivities of our \hisd\ and \htwosd\ measurements.  The same conclusion
    was reached by \citet{lee12} for Perseus, where \htwosd\ was found to extend
    up to $\sim$20 pc from the centers of B5 and NGC 1333.  Taking advantage of
    our much wider \hisd\ and \htwosd\ images, we tested several larger regions
    extending all the way up to $\sim$9.5$\circ$ or $\sim$50 pc from the centers
    of the individual cores and still did not detect purely atomic envelopes.
    This result suggests that a large amount of diffuse \htwo may exist around
    molecular clouds, which is supported by a recent study by
    \citet{roman-duval16} where a significant fraction of CO emission in the
    Galaxy was found to originate from diffuse molecular gas.  

    %(from $\sim$10--20\% at Galactocentric radii of $\sim$3--4 kpc to
    %$\sim$50\% at
    %15 kpc). 

  \subsubsection{Importance of  \hi-dust Shielding}
  \label{model_results3} 

    By fitting \rhtwo as a function of \hsd, we constrain $\phi_{\rm CNM}$ of
    $\sim$2--3, $\sim$3--4, and $\sim$3--9 for California, Perseus, and Taurus
    respectively, which are in agreement with the expected range of $\sim$1--10
    (K+09).  These $\phi_{\rm CNM}$ values of $\sim$2--9 in turn correspond to
    large $\chi$ $\sim$ 4--13.  Similarly, we find large \aG\ $\sim$ 6--40 from
    the best-fit S+14 models, with Taurus showing the most significant spatial
    variation (a factor of $\sim$6).  Note that a factor of $\sim$2 variation in
    \hisd\ across Taurus becomes more pronounced in \aG\ than in $\chi$ (a factor
    of $\sim$6 vs. a factor of $\sim$3 variations), because of the logarithmic
    dependence of \hisd\ on \aG\ (\autoref{eq:s14_Sigma_HI}).  Our finding of
    large $\chi$ and \aG\ values suggest that dust grains associated with \hi
    (\hi-dust) dominate shielding of \htwo molecules against dissociating FUV
    photons.  While \citet{lee12,lee15} reached the same conclusion for Perseus,
    our extended work implies that \hi-dust absorption may be the major
    shielding component for \htwo formation in the solar neighborhood. 

    We note that the constrained $\chi$ of $\sim$9--13 for Perseus is much
    larger than what \citet{lee12} found ($\chi$ $\sim$ 1--2).  This difference
    primarily arises from the fact that we use $\sigma_{\rm g,-21}$ $\sim$ 4.1
    for Perseus (properly scaled based on the measured DGR), which is a factor
    of $\sim$4 higher than what \citet{lee12} adopted for their model fitting. 

  \subsubsection{Multi-phased \hi Envelopes} 

    Finally, we compare the model-based $n_{\rm CNM}$ (K+09) and $n$ (S+14)
    values with the observational constraints provided by
    \citet{stanimirovic14}.  In essence, \citet{stanimirovic14} examined the \hi
    emission and absorption spectra obtained toward 26 radio continuum sources
    behind Perseus (scattered over a large area of $\sim$45$^{\circ}$ $\times$
    10$^{\circ}$ centered on Perseus) and found that the spin temperature
    ($T_{\rm s}$) of 107 individual \hi components ranges from $\sim$10 K to
    $\sim$200 K with a median of $\sim$50 K.  To compare with the model
    predictions, we then convert the individual $T_{\rm s}$ into the density
    $n_{\rm s}$ by assuming a standard thermal pressure of $p/k$ = $T_{\rm
    s}$$n_{\rm s}$ = 3800 K cm$^{-3}$ (\citeauthor{jenkins11} 2011).  In
    addition, we derive the average $<n_{\rm s}>$ along each line-of-sight by
    using the optical-depth-weighted spin temperature $<T_{\rm s}>$ from
    \citet{stanimirovic14}: 
    
    \begin{equation}
      <T_{\rm s}> = \frac{\int \tau(v) 
      \frac{T_{\rm exp}(v)}{(1 - e^{-\tau(v)})} 
        \delta v}{\int \tau(v) \delta v}, 
    \end{equation} 

    where $\tau(v)$ is the optical depth spectrum and $T_{\rm exp}(v)$ is the
    ``expected'' emission spectrum (\hi spectrum that we would observe at the
    source location if the continuum source were turned off).  The cumulative
    distribution functions of $n_{\rm s}$ and $<n_{\rm s}>$ are presented in
    \autoref{fig:density_cdf}, along with those of K+09 $n_{\rm CNM}$ and S+14
    $n$. 

    First of all, we find that $n_{\rm CNM}$ has a median value of 20 cm$^{-3}$,
    which is a factor of $\sim$4 lower than the median $n_{\rm s}$ of 75
    cm$^{-3}$.  In addition, the distribution of $n_{\rm CNM}$ is much narrower,
    reflecting the small range of the observed \hisd: the standard deviations of
    $n_{\rm CNM}$ and $n_{\rm s}$ are 7 cm$^{-3}$ and 236 cm$^{-3}$
    respectively.  These different distributions of $n_{\rm CNM}$ and $n_{\rm
    s}$ mainly result from K+09's assumption that all dust shielding for \htwo
    formation is associated with the CNM.  In reality, \hi is multi-phased
    (e.g., the CNM fraction in and around Perseus is $\sim$ 30\%;
    \citeauthor{stanimirovic14} 2014) and dust grains associated with \htwo
    molecules can also provide additional shielding.  On the other hand, the
    line-of-sight average density $<n_{\rm s}$ has a similar distribution as
    $n_{\rm CNM}$: the median of 20 cm$^{-3}$ and the standard deivation of 9
    cm$^{-3}$.  While $n_{\rm s}$ is almost entirely for the CNM, $<n_{\rm s}>$
    takes into account warmer and more diffuse \hi components.  The good
    agreement between the $n_{\rm CNM}$ and $<n_{\rm s}>$ distributions hence
    further supports that \hi in and around California, Perseus, and Taurus is
    multi-phased.  We note, however, that $<n_{\rm s}>$ is still biased toward
    the CNM.  This can be inferred by the the relatively low $<T_{\rm s}>$ of
    $\sim$80--520 K, which is in contrast with the fact that the CNM contributes
    only a small fraction of the total column density ($\sim$30\% on average)
    along each line-of-sight. 

    %(e.g., only 8 out of the total 26 lines-of-sights have $<T_{\rm s}>$ > 200
    %K, a conservative threshold for the CNM; \citeauthor{kim14} 2014). 

    With a low median value of 3 cm$^{-3}$, the distribution of $n$ is
    distinctly different from the rest of the density histograms in Figure
    \autoref{fig:density_cdf}.  As described in
    \autoref{sec:sec:k09_s14_compare}, S+14 built a general theory of \htwo
    formation without any assumption of \hi phases, thus constraining the
    effective \hi density.  The effective $n$ $\sim$ 3 cm$^{-3}$ is translated
    into a line-of-sight depth of $\sim$100 pc when the median \hisd\ $\sim$
    8\,\msunpc\ is considered (\autoref{table:core_properties})  and this length
    scale in fact agrees well with the characteristic size of \hi envelopes
    ($\sim$80 pc) \citet{imara11} measured for several Galactic molecular clouds
    based on kinematic analyses of \hi emission.  This result suggests that S+14
    $n$ is indeed a representative density for the \hi envelopes surrounding
    California, Perseus, and Taurus.  Interestingly, $n$ $\sim$ 3 cm$^{-3}$ is
    comparable to what \citet{sofue16} recently found as the threshold density
    for the \hi-to-\htwo transition in the Galaxy ($\sim$4 cm$^{-3}$),  as well
    as the density expected for the thermally unstable medium (UNM) (e.g.,
    \citeauthor{wolfire03} 2003), suggesting that the transition from the WNM to
    CNM may have a link with \htwo formation. 

      \begin{deluxetable*}{lcc|cccc|cccc}[!ht]
        \tabletypesize{\footnotesize}
        \tablecolumns{11}
        %\tablewidth{0pt}

        \tablecaption{Core Properties \label{table:core_properties}}

        \tablehead{
          \colhead{Cloud} & 
          \colhead{Core} & 
          \colhead{Alternate} &
          \colhead{\phicnm} &
          \colhead{$\chi$} &
          \colhead{\hisd\ K+09} &
          \colhead{\ncnm} &
          \colhead{\aG} &
          \colhead{$\chi^\prime$} &
          \colhead{\hisd\ S+14} &
          \colhead{$n$} \\
          \colhead{Name} &
          \colhead{Name} &
          \colhead{Name} &
          \colhead{} &
          \colhead{} &
          \colhead{[\msunpc]} &
          \colhead{[cm$^{-3}$]} &
          \colhead{} &
          \colhead{} &
          \colhead{[\msunpc]} &
          \colhead{[cm$^{-3}$]}
           }

        \startdata{}

          \input{tables/modelparams_table.tex}
        
        \enddata{}

        \tablecomments{Core names are from the C3PO by \citet{planck11_23}. 
          In addition, alternative names from \citet{lombardi10} and \citet{bally12} are provided for several core regions. 
          %Core name: Cold clump name from C3PO \citet{planck11_23}.
          %We associated alternate names of core regions from \citet{lombardi10} and
          %\citet{bally12}.  
          %\phicnm\ (K+09 model) represents the ratio between the
          %observed CNM volume density and the CNM volume density necessary to remain
          %in pressure equilibrium with the WNM.\@ $\chi$ is the normalized radiation
          %field (\autoref{eq:k09_new_chi}).  \hisd\ K+09 is the \hisd\ value where
          %the K+09 model \rhtwo\ is 1. \ncnm\ is the neutral hydrogen volume density
          %predicted from K+09 determined from \autoref{eq:k09_nCNM}. \aG\ (S+14
          %model) represents the dust-absorption efficiency of \htwo\-dissociating
          %photons.  $\chi^\prime$ is the normalized radiation field with an
          %additional contribution from \htwo-associated dust
          %(\autoref{sec:sternberg_model}).  The \hisd\ S+14 value is the \hisd\ at
          %the \hitohtwo\ transition. $n$ is the neutral hydrogen volume density
          %predicted from S+14 determined from \autoref{eq:s14_alphaG}. All errors
          %are shown at 68\% confidence. See \autoref{sec:model_descriptions} for a
          %description of the model parameters. All properties are reported to with
          % 2 significant digits.
          }

      \end{deluxetable*}


    \begin{figure}[!ht]
      \includegraphics[width=\linewidth]{figures/california_hisd_vs_hsd.pdf}

      \caption{\label{fig:california_hi_vs_h} Plot of \hisd\ vs. \hsd\ for
        individual cores in California.\@ The contours represent the densities
        of data points increasing logarithmically. 
        %The measurement of each axis is
        %independent where \hi\ observations were used for \hisd\ and $A_V$
        %measurements used to trace \hsd. 
        Each core region contains 180 independent lines of sight.  The best-fit
        K+09 and S+14 models are shown as solid lines and their parameters are
        summarized in \autoref{table:core_properties}.  The systematic
        uncertainties of our \hisd\ and \hsd\ measurements are $\sim$3\,\msunpc
        and $\sim$5\,\msunpc\ respectively. 
        %The model fits for the S+14 and K+09 models are shown as
        %solid lines.  The 68\%-confidence uncertainty on the \hisd\ threshold in
        %California is about 1\,\msunpc. The best-fit parameters for both the S+14
        %model and K+09 models are shown in \autoref{table:core_properties}, as
        %well as the 68\% confidence interval on the \hisd\ threshold. The
        %systematic uncertainty on \hisd\ is about 3\,\msunpc, and 5\,\msunpc\ for
        %\hsd.
      }

    \end{figure}

    \begin{figure}[!ht]
      \includegraphics[width=\linewidth]{figures/perseus_hisd_vs_hsd.pdf}

      \caption{\label{fig:perseus_hi_vs_h} Same as
        \autoref{fig:california_hi_vs_h}, except that the data points are from
        individual cores in Perseus.  The systematic uncertainties of our \hisd\
        and \hsd\ measurements are $\sim$1\,\msunpc and $\sim$5\,\msunpc\
        respectively.
        %Plot of \hisd\ vs. \hsd\ for individual
        %cores in Perseus. The 68\%-confidence uncertainty on the \hisd\ threshold
        %in Perseus is about 0.3\,\msunpc. The systematic uncertainty on \hisd\ is
        %about 1\,\msunpc, and 5\,\msunpc\ for \hsd.
      }

    \end{figure}

    \begin{figure}[!ht]
      \includegraphics[width=\linewidth]{figures/taurus_hisd_vs_hsd.pdf}

      \caption{\label{fig:taurus_hi_vs_h} Same as
        \autoref{fig:california_hi_vs_h}, except that the data points are from
        individuala cores in Taurus.  The systematic uncertainties of our \hisd\
        and \hsd\ measurements are $\sim$3\,\msunpc and $\sim$5\,\msunpc\
        respectively.
        %Plot of \hisd\ vs. \hsd\ for individual
        %cores in Taurus. The 68\%-confidence uncertainty on the \hisd\ threshold
        %in Taurus is about 0.5\,\msunpc. The systematic uncertainty on \hisd\ is
        %about 3\,\msunpc, and 5\,\msunpc\ for \hsd.
      }

    \end{figure}

    \begin{figure}[!ht]
      \includegraphics[width=\linewidth]{figures/california_rh2_vs_hsd.pdf}

      \caption{\label{fig:california_rh2_vs_h} Plot of \rhtwo\ vs. \hsd\ for
        individual cores in California.  The best-fit K+09 and S+14 models are
        overlaid as solid lines and their parameters are presented in
        \autoref{table:core_properties}.  
        %i.e., \hi-\htwo\ transition profiles.  The
        %model fits for the S+14 and K+09 models are shown as solid lines.  The
        %best-fit parameters for both the S+14 model and K+09 models are shown in
        %\autoref{table:core_properties}, as well as the 68\% confidence interval
        %on the \hisd\ transition.
      }

    \end{figure}

    \begin{figure}[!ht]
      \includegraphics[width=\linewidth]{figures/perseus_rh2_vs_hsd.pdf}

      \caption{\label{fig:perseus_rh2_vs_h} Same as
        \autoref{fig:california_rh2_vs_h}, except that the curves are for
        individual cores in Perseus. 
       %Plot of \rhtwo\ vs. \hsd\ for
        %individual cores in Perseus.
      }

    \end{figure}

    \begin{figure}[!ht]
      \includegraphics[width=\linewidth]{figures/taurus_rh2_vs_hsd.pdf}

      \caption{\label{fig:taurus_rh2_vs_h} Same as
        \autoref{fig:california_rh2_vs_h}, except that the curves are for
        individual cores in Taurus.
        %Plot of \rhtwo\ vs. \hsd\ for
        %individual cores in Taurus.
      }

    \end{figure}

    %\begin{comment}

    %\subsection{Error from Region Selection}

    %  We use the isolated core G160.49-16.81 as an test subject for our systematic
    %  uncertainty in region selection. We perform the same monte carlo simulation
    %  outlined in \autoref{sec:monte_carlo_simulations}, except that we
    %  randomly rotate the core region about the core position. We find that the
    %  systematic uncertianties in region selection have similar value as the other
    %  factors accounted for in \autoref{sec:nhi_nH2_dgr}.

    %cloud,core,ra,dec,phi_cnm,phi_cnm_error_low,phi_cnm_error_high,alphaG,alphaG_error_low,alphaG_error_high,hi_trans,hi_trans_error_low,hi_trans_error_high


    %perseus,G160.49-16.81 
    %\phicnm\ = 8.39^{+1.07}_{-0.63}
    %\aG\ = 2.93^{+0.41}_{-0.34}

    %perseus,G160.49-16.81,56.899969479093535,32.916064344285694,8.392737089069533,0.6321478768962665,1.0789649380479869,2.933600840134079,0.33645555289518114,0.41655896816680027,,,

    %without rotation:
    %\phicnm\ = 9.73^{+0.75}_{-0.36}
    %\aG\ = 2.91^{+0.10}_{-0.29}

    %\end{comment}
