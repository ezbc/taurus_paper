# Taurus-California-Perseus HI-to-H2 Transition Manuscript #

## Organization

The sections of the paper are kept in the ``sections`` folder. Tables and figures have their own folder as well. The paper uses bibtex to manage the references, so new bibliographic references should be added to ``my_bib.bib``.

## Compiling

Bibtex provides an easy way to manage references in latex. A great [tutorial](http://www.latex-tutorial.com/tutorials/beginners/lesson-7/) can get one started with bibtex quickly. 

In order to compile this document, run the bash script ``texit`` as follows

```
#!bash
./texit <filename>
```

This requires the pdflatex and bibtex programs. An explanation of what this bash script does is below.

### Explanation of compilation

The latex file needs to be compiled a total of four times in the following manner

```
#!bash
pdflatex <filename_base>.tex
bibtex <filename_base>
pdflatex <filename_base>.tex
pdflatex <filename_base>
```

The first ``pdflatex`` command tells bibtex what literature we cited in our
paper. The ``bibtex`` command creates a ``.bib`` file which will be translated
into the references section. The next two steps merge the reference section with
the latex document and then assigns successive numbers in the last step.


# Location of Files

## Analysis 
Location of analysis scripts: `/usr/users/ezbc/research/scripts/multicloud/analysis/clouds`

+ `cloud_bootstrapping.py` runs the main analysis.

+ `cloud_write_parameter_table.py` collects the MC simulation results and writes
    the model parameter table.

+ `cloud_plot_model_param_map.py` plots the density CDFs and the HI threshold
    map.

## Data Products

The final data products are located at
`/d/bip3/ezbc/multicloud/data/final_products/`

A summary of the observed maps including masses, region sizes, and uncertainty
statistics is located at:
`/d/bip3/ezbc/multicloud/data/python_output/tables/cloud_obs_stats.csv`

## Data

Below are directories for Perseus. California and Taurus have the same directory
and file naming structure.

The scripts used for reprojecting the data are:

+ Galfa cube combine `/usr/users/ezbc/research/scripts/perseus/reduction/perseus_reduction_galfa_combine.csh`

+ Planck data extraction `/usr/users/ezbc/research/scripts/perseus/reduction/perseus_reduction_planck_extraction.py`

+ Data regridding `/usr/users/ezbc/research/scripts/perseus/reduction/perseus_reduction_map_regrids.py`

The data used in the analysis, Av, and HI, are in the following directories:

Planck Av
+ Data `/d/bip3/ezbc/perseus/data/av/perseus_av_planck_tau353_5arcmin.fits`
+ Error `/d/bip3/ezbc/perseus/data/av/perseus_av_error_planck_tau353_5arcmin.fits`

K+09 Av
+ Data `/d/bip3/ezbc/perseus/data/av/perseus_av_k09_regrid_planckres.fits`

Galfa DR2 HI
+ Data
  `/d/bip3/ezbc/perseus/data/hi/perseus_hi_galfa_cube_regrid_planckres.fits`
+ Error 
  `/d/bip3/ezbc/perseus/data/hi/perseus_hi_galfa_cube_regrid_planckres_noise.fits`

Planck Tdust
+ Data `/d/bip3/ezbc/multicloud/data/dust_temp/multicloud_dust_temp_5arcmin.fits`
+ Error `/d/bip3/ezbc/multicloud/data/dust_temp/multicloud_dust_temp_error_5arcmin.fits`

Planck Beta
+ Data `/d/bip3/ezbc/multicloud/data/dust_temp/multicloud_dust_beta_5arcmin.fits`
+ Error `/d/bip3/ezbc/multicloud/data/dust_temp/multicloud_dust_beta_error_5arcmin.fits`


The DS9 region files are located at
`/d/bip3/ezbc/multicloud/data/python_output/regions/multicloud_divisions.reg`


