California
------------------------------------------------------------------------------
L1482
-----
Onisha+02
    http://adsabs.harvard.edu/abs/2015MNRAS.449.3867G
Li+14
    http://adsabs.harvard.edu/abs/2014A%26A...567A..10L
Kong+15
    http://adsabs.harvard.edu/abs/2015ApJ...805...58K


L1483
-----
Lada+09
    http://adsabs.harvard.edu/abs/2009ApJ...703...52L

Connelley+08
    http://adsabs.harvard.edu/abs/2008AJ....135.2496C

    + Lbol = 1.7 Lsun
    + YSO present


Taurus
------------------------------------------------------------------------------
L1495
-----
http://adsabs.harvard.edu/abs/2015ApJ...805..185S
    + NH_3 obs, likely unbound cores

    + The L1495-B218 filaments form an interconnected, nearby, large complex
    extending over 8 pc

http://adsabs.harvard.edu/abs/2015ApJ...807..119R

    + Taurus is rotating with v = 5 km/s

http://adsabs.harvard.edu/abs/2015A%26A...574A.104T

    + L1495/B213 originated from the supersonic collision of two flows. The
    collision produced a network of intertwined subsonic filaments or fibers
    (fray step). Some of these fibers accumulated enough mass to become
    gravitationally unstable and fragment into chains of closely-spaced cores.

    + 

http://adsabs.harvard.edu/abs/2013A%26A...554A..55H

    + observed L1495/B213 in C18O(1-0), N2H+(1-0), and SO(JN = 32-21) with the
    14 m FCRAO telescope, and complemented the data with dust continuum
    observations using APEX (870 μm) and IRAM 30 m (1200 μm)

    + Core formation in L1495/B213 has proceeded by hierarchical fragmentation.
    The cloud fragmented first into several pc-scale regions. Each of these
    regions later fragmented into velocity-coherent filaments of about 0.5 pc in
    length. Finally, a small number of these filaments fragmented
    quasi-statically and produced the individual dense cores we see today.

http://adsabs.harvard.edu/abs/2010ApJ...725.1327S

    + devoid of YSOs

B213
----
http://adsabs.harvard.edu/abs/2012ApJ...756...12L

    + cyanoacetylene (HC3N)  obs

    +  The line width and velocity gradient seen in HC3N are also consistent
    with Taurus B213 being a self-gravitating filament in the early stage of
    either fragmentation and/or collapse

B217
----
understudied

http://adsabs.harvard.edu/abs/2001A%26A...372..302H

    + Knowing the ammonia abundance, we calculate the thermal, turbulent and
    gravitational energies of the dense core, which appears to be close to
    hydrostatic equilibrium. Our results are compatible with B217SW being now on
    the verge of collapse or in an early collapse phase

    + he low dust temperatures cannot be explained by attenuation of the
    interstellar radiation field alone and may reflect a change in the optical
    properties of the dust as compared to diffuse clouds. In B217SW, molecular
    depletion through freeze-out onto grains is suggested by the comparison of
    our FIR and NH3 data with previous C18O observations

B215
----
not much...

L1524
-----
should be G172.12-16.94
http://adsabs.harvard.edu/abs/2012AJ....144..143B

    + 

http://adsabs.harvard.edu/abs/2002ApJ...575..950O

    + molecular condensation timescales about 10^4 years


L1504
-----
should be G171.14-17.57

L1535
-----
should be G174.70-15.47, replace with B18

http://adsabs.harvard.edu/abs/2012AJ....144..143B

    +  L1535 is shown to power a giant outflow at least 5.5 pc in length 


B18
---
should be G172.93-16.73. replace L1524

http://adsabs.harvard.edu/abs/2012AJ....144..143B

    + contains a concentration of a dozen young stars

    + The B18 cloud is one of the most active sites of protostellaroutflow
    production in Tauru

    + young stars in B18 drive aplethora of other HH objects







