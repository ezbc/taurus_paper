#!/usr/bin/python

import os


DIR_DEST = './figures/'
DIR_TABLE = '/d/bip3/ezbc/multicloud/data/python_output/tables/'
DIR_FIGURE = '/d/bip3/ezbc/multicloud/figures/'

images = [
'maps/av_map.pdf',
'maps/multicloud_av_cores_map.pdf',
#'maps/multicloud_av_modelparams_map.pdf',
'maps/multicloud_av_ISMparams_map.pdf',
#'maps/multicloud_dust_temp_map.pdf',
'maps/multicloud_rad_field_map.pdf',
'maps/hi_h2_maps.pdf',
'maps/fraction_diffuse_map.pdf',
'spectra/multicloud_spectra.pdf',
'av_nhi/multicloud_av_vs_nhi.pdf',
#'pdfs/multicloud_pdfs.pdf',
'models/california_hisd_vs_hsd.pdf',
'models/taurus_hisd_vs_hsd.pdf',
'models/perseus_hisd_vs_hsd.pdf',
'models/california_rh2_vs_hsd.pdf',
'models/taurus_rh2_vs_hsd.pdf',
'models/perseus_rh2_vs_hsd.pdf',
'models/multicloud_T_cnm_vs_gdist.pdf',
'temps/density_cdf.pdf',
'temps/modelparams_cdf.pdf',
        ]

for image in images:
    os.system('cp ' + DIR_FIGURE + image + ' ' + DIR_DEST)

tables = ['modelparams_table.tex',
          #'modelparams_table_appendix.tex',
          'multicloud_vel_ranges.tex',
          'cloud_params_table.tex',
          'cloud_obs_stats.csv',
          'multicloud_hi_core_properties.tex',
          'multicloud_model_params.tex',
          ]

for table in tables:
    os.system('cp ' + DIR_TABLE + table + ' ./tables/')

