
- Introduction needs to be updated. You also need to introduce here the 3 main
  GMCs we are studying and provide some of the basic properties. For example, a
  lot of information from Table 3 should go here. Anything that comes from
  previous work/publications should go into Introduction (e.g. distance, mass,
  YSOs). You need to explain and briefly compare these properties, and also in
  1-2 sentences say how were these values derived so it is clear from somebody
  from outside what's going on.

- Another common comment: many sections do not flow well and also it is not very
  clear what has been done to the data. Can you please go through the paper and
  try to explain things better. You do not need too much more text, but it needs
  to clear what/how has been done.  Please use Lee et al. 2012 as an example of
  how clear/logical explanations have to be.

- common issue: "data" is plural, so "data are" NOT "data is"

- Another common problem: any figure and table has to be described well in the
  text, and then you need to say what is the conclusion/meaning of the
  figure/table shows.

- 2nd para in Section 3: text like this should go at the end of Introduction,
  not half way through the paper. Again, see Lee et al. 2012

- Gaussian should be Gaussian function

- Section 3.2 - from reading the text it is not clear what exactly was done

- Section 3.3 should go under Data where you talk about dust observations and
  products.

- section 3.4 - DGR: At the end of this section you need some
  explanation/summary of what your results mean. How do derived values compare
  with the typical MW DGR? Are values consistent with Lee et al. 2012 or not?
  Are our values reasonable or not?

- Table 3: if uncertainties are symmetric relative to the most probable value
  then give just one value, e.g. +/-0.3

- section 4.1 repeats the same text twice

- Most of Section 4.2 talks about previous work, this should go into Introduction.

You talk here about Sigma_HI but you never say how was this estimated (mean
value? fitted value?)

- Section 5: I think it would be good to show 2 dust temperature images.  We
  also need to comment on internal ISRF. It is not clear how exactly you
  measured Tdust per region (focusing on external areas). If dust is primarily
  heated by an external ISRF then mean/median values of each large region may be
  a good representation of Tdust.

- Table 4: you mix parameters from 2 models. Maybe have a vertical line to
  separate 2 models and group parameters appropriately.

- Explain results from Table 3 and how they compare with Lee et al. 2012.

- Figures 6-8: I think it's strange not to show data uncertainties but to show
  68% confidence range for the fitted model results. I think data points and
  uncertainties are more trustworthy than the derived model parameters which
  require lots of assumptions.

I think it would be better (more important) to show uncertainties on individual
data points and then just the best model fits, and then give 68% confidence
intervals in Table 4. Min, what are your thoughts on this?

- Model comparison: I think it would be great to make a plot for 1-2 cores where
  have a large dynamic range and show how the 2 models compare. There are some
  important differences that we can describe, e.g. S+14 has a sharper transition
  and a flatter Sigma_HI than K+09. It would be good to see if our data can
  distinguish between the models. If not, we can say that distinguishing between
  models is impossible currently. But we should try.

A question: are models fitted to Sigma_HI vs Sigma_tot, or R(H2) vs Sigma_tot?
If yes, then maybe we should show some of those plots as well?

- Figure 10: split results into 2 figures as current one is mixing parameters
  from 2 models.

- Discussion section has good topics, e.g. observed temperature comparison with
  models (Fig. 9) but it has to be written better. Please check Lee et al. 2012
  and try to have a go with re-writing this section.

- With 30 cores shown in table 4, maybe we could plot model parameters as a
  function of Galactic latitude (distance from the plane)?  A single plot per
  model can show several quantities using different symbols.



